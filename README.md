# Salvation

## Contributors

* **Bryan Curchod** - *Frontend / Mobile App / Testing App* - [BryCur](https://gitlab.com/BryCur)
* **Daniel Gonzalez Lopez** - *Backend / Mobile App* - [Angorance](https://gitlab.com/Angorance)
* **François Burgener** - *Frontend / Mobile App* - [burgenerfrancois](https://gitlab.com/burgenerfrancois)
* **Héléna Reymond** - *Backend (all services)* - [LNAline](https://gitlab.com/LNAline)

## Description

This project was realized as part of our studies at [HEIG-VD](https://heig-vd.ch/).  

The goal was to implement some services beeing part of a smart student card. We chose to make them as microservices.  
Each microservice exposes a REST API that we use in our front-end. Here are the microservices we implemented:
* Authentication service, to be able to authenticate a user and deliver a JsonWebToken to identify him in other services.
* Profile service, to manage user's data, get it or modify it.
* Wallet service, to be able to make transactions (payments) with the smart card or transfers from user to user.
* Parking service, to be able to pay the parking fee from a mobile app and store the licence plates of users who paid.

To be able to use these services, we had to implement some front-end. On this side, we decided to make:
* Web app, with a user and an admin access.
* Mobile app, mostly to view the data, but with the ability to make a transfer from user to user and to pay the parking.

For the technology part, we chose to make our microservices with SailsJS (NodeJS framework) and a MongoDB Database, each service running in a Docker container.  
All of them defined in a `docker-compose.yml` file, to be able to launch them all easily. The containers were hosted on the Google Cloud Platform in a Debian VM.  
For the web app, we decided to use Angular 6 (JavaScript framework using TypeScript) to have a structured web app with Material Design.  
For the mobile app, we used Flutter (Google's Mobile framework using Dart language) for its cross-platform capabilities.

## Structure

### Frontend
* sal_w_app

### Backend
* sal_authentication
* sal_profile
* sal_wallet
* sal_parking

### Mobile App
* sal_m_app

### Testing app
* sal_m_checkout