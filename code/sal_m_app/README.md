# Mobile App With Flutter

## Introduction
The mobile app is a useful tool in our service, it allows the user to have the main features in his pocket. The app isn't made for modifying user's data but mostly to show his useful data (main wallet, profile, ...) and to access the main features of our services (transfer money to a user, pay parking).

This way, the user has his life simplified as he can do lots of things on the go.

 ## Technology

We made this mobile application with Flutter. It is based on Dart, Google's programming language, and is a quite interesting technology. It allows devlopers to build native iOS and Android apps with a single language. No need to learn both Android and iOS languages to do that.

Flutter is brought by Google, is now in v1.0 and guarantees high performances in both OS.

 ## Structure

The app is made of two main parts, the login page and the connected zone.

The login page is very simple, a form with two text inputs and a button.  

The connected zone is made of several pages with a drawer to change from one to another.

* Dashboard
* Profile
* Wallet
* Transfer Money (from user to user)
* Parking Payment
* Log out (only in drawer)

## Presentation of our application

![login](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/login.jpg)

![drawer](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/drawer.jpg)

![dashboard](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/dashboard.jpg)

![profile](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/profile.jpg)

![wallet](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/wallet.jpg)

![transfer u2u](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/transfer.jpg)

![parking payment](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal_m_app/screenshots/parking.jpg)

## What can we do with it?

A user, after authenticating, can:

* See his profile data (address, licence plate, ...)
* See the state of his main wallet and the last transactions
* Transfer money to another user
    * Simply using the other user's email address
* Pay the parking fee
    * If a licence plate is registered, simply by pressing the button
    * Otherwise, it has to enter the licence plate in the field before

