///Data Wallet class
class WalletData {
  String id;
  double amount;
  List<Transaction> transactions = new List();

  static final WalletData _instance = new WalletData._internal();

  static WalletData get instance => _instance;

  WalletData._internal();

  void setWalletData(String id, double amount) {
    this.id = id;
    this.amount = amount;
  }

  void setFromJson(List list) {
    setWalletData(list[0]['_id'] != null ? list[0]['_id'] : "",
        list[0]['amount'] != null ? list[0]['amount'] / 1.0 : 0.0);
  }

  void setTransactions(List list) {
    for (Map item in list) {
      Transaction tr = new Transaction();

      tr.setFromJson(item);

      transactions.add(tr);
    }

    print(transactions);
  }

  void clear() {
    id = null;
    amount = null;
    transactions = null;
  }
}

class Transaction {
  String to;
  String from;
  double amount;

  void setTransactionData(String to, String from, double amount) {
    this.to = to;
    this.from = from;
    this.amount = amount;
  }

  void setFromJson(Map<String, dynamic> json) {
    setTransactionData(
        json['to']['name'] != null ? json['to']['name'] : "",
        json['from']['name'] != null ? json['from']['name'] : "",
        json['amount'] != null ? json['amount'] / 1.0 : 0.0);
  }
}
