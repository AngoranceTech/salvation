import 'dart:math';

/// Data profile class
class ProfileData {
  String token;
  String firstName;
  String lastName;
  String birthdate;
  String address;
  int npa;
  String city;
  String phoneNumber;
  String email;
  String numberplate;
  String profilePic;

  static final ProfileData _instance = new ProfileData._internal();

  static ProfileData get instance => _instance;

  ProfileData._internal();

  void setProfileData(
      String firstName,
      String lastName,
      String birthdate,
      String address,
      int npa,
      String city,
      String phoneNumber,
      String email,
      String numberplate) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthdate = birthdate;
    this.address = address;
    this.npa = npa;
    this.city = city;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.numberplate = numberplate;

    Random rand = new Random();

    if (firstName == 'François' && lastName == 'Burgener') {
      this.profilePic = 'assets/images/profilePic2.gif';
    } else {
      this.profilePic =
          'assets/images/profilePic' + rand.nextInt(4).toString() + '.gif';
    }
  }

  void setToken(String token) {
    this.token = token;
  }

  void setFromJson(Map<String, dynamic> json) {
    setProfileData(
        json['first_name'] != null ? json['first_name'] : "",
        json['last_name'] != null ? json['last_name'] : "",
        json['birth_date'] != null ? json['birth_date'] : "",
        json['address'] != null ? json['address'] : "",
        json['npa'] != null ? json['npa'] : -1,
        json['city'] != null ? json['city'] : "",
        json['phone_number'] != null ? json['phone_number'] : "",
        json['email'] != null ? json['email'] : "",
        json['plate_number'] != null ? json['plate_number'] : "");
  }

  void clear() {
    firstName = null;
    lastName = null;
    birthdate = null;
    address = null;
    npa = null;
    city = null;
    phoneNumber = null;
    email = null;
    numberplate = null;
  }
}
