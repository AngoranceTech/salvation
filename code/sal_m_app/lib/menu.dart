import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:sal_m_app/themes/mainTheme.dart';
import 'package:sal_m_app/persistentData/profileData.dart';
import 'package:sal_m_app/utilities/function.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Menu extends StatelessWidget {
  final _token = ProfileData.instance.token;

  @override
  Widget build(BuildContext context) {
    ProfileData _profile = ProfileData.instance;

    final String _header = _profile.firstName + " " + _profile.lastName;

    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the Drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(_header),
            decoration: BoxDecoration(
              color: salvationTheme.primaryColor,
            ),
          ),
          ListTile(
            title: Text('Dashboard'),
            trailing: new Icon(
              Icons.dashboard,
              color: Colors.black,
            ),
            onTap: () {
              updateWallet();

              Navigator.pushNamed(context, '/dashboard');
            },
          ),
          ListTile(
            title: Text('Profile'),
            trailing: new Icon(
              Icons.person,
              color: Colors.black,
            ),
            onTap: () {
              updateWallet();

              Navigator.pushNamed(context, '/profile');
            },
          ),
          ListTile(
            title: Text('Wallet'),
            trailing: new Icon(
              Icons.account_balance,
              color: Colors.black,
            ),
            onTap: () {
              updateWallet();

              Navigator.pushNamed(context, '/wallet');
            },
          ),
          ListTile(
            title: Text('Transfer money'),
            trailing: new Icon(
              Icons.swap_horiz,
              color: Colors.black,
            ),
            onTap: () {
              updateWallet();

              Navigator.pushNamed(context, '/transferMoney');
            },
          ),
          ListTile(
            title: Text('Parking'),
            trailing: new Icon(
              Icons.time_to_leave,
              color: Colors.black,
            ),
            onTap: () {
              updateWallet();

              Navigator.pushNamed(context, '/parking');
            },
          ),
          ListTile(
            title: Text('Log out'),
            trailing: new Icon(
              Icons.power_settings_new,
              color: Colors.black,
            ),
            onTap: () async {
              final prefs = await SharedPreferences.getInstance();

              prefs.clear();
              clearData();

              Navigator.pushNamed(context, '/login');
            },
          ),
        ],
      ),
    );
  }

  void updateWallet() {
    getDataWallet(_token).then((result) {
      if (result['status'] == 200) {
        print('Amount updated');
      }
    });
  }
}
