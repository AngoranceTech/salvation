import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sal_m_app/persistentData/profileData.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

// Based on tutorial: https://www.youtube.com/watch?v=K2cyOZtQvwY
class _ProfilePageState extends State<ProfilePage> {
  final ProfileData _profileData = ProfileData.instance;

  @override
  Widget build(BuildContext context) {
    final String _name = _profileData.firstName + ' ' + _profileData.lastName;
    final String _address = _profileData.address +
        ', ' +
        _profileData.npa.toString() +
        ', ' +
        _profileData.city;

    final double _height = MediaQuery.of(context).size.height;
    final double _width = MediaQuery.of(context).size.width;

    return new Stack(children: <Widget>[
      Image.asset('assets/images/profileHeader.jpeg'),
      Container(
        child: ClipPath(
          child: Container(color: Color.fromARGB(255, 250, 250, 250)),
          clipper: SlashClipper(),
        ),
      ),
      Positioned(
          width: _width,
          top: _height / 6,
          child: Column(children: <Widget>[
            Container(
              width: _width / 3.5,
              height: _width / 3.5,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(_profileData.profilePic),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.all(Radius.circular(75.0)),
                  boxShadow: [
                    BoxShadow(blurRadius: 7.0, color: Colors.black),
                  ]),
            ),
            SizedBox(height: 20.0),
            Text(_name,
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'MontSerrat',
                )),
            SizedBox(height: 3.0),
            Text(_profileData.email,
                style: TextStyle(
                  fontSize: 15.0,
                  fontStyle: FontStyle.italic,
                  fontFamily: 'MontSerrat',
                )),
          ])),
      ListView(
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          SizedBox(height: _height / 2),
          ListTile(
            leading: Icon(Icons.home),
            title: Text(
              'Address',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'MontSerrat',
              ),
            ),
            subtitle: Text(
              _address,
              style: TextStyle(
                fontSize: 18.0,
                fontFamily: 'MontSerrat',
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.phone),
            title: Text(
              'Phone Number',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'MontSerrat',
              ),
            ),
            subtitle: Text(
              _profileData.phoneNumber,
              style: TextStyle(
                fontSize: 18.0,
                fontFamily: 'MontSerrat',
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.time_to_leave),
            title: Text(
              'Plate Number',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'MontSerrat',
              ),
            ),
            subtitle: Text(
              _profileData.numberplate,
              style: TextStyle(
                fontSize: 18.0,
                fontFamily: 'MontSerrat',
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.cake),
            title: Text(
              'Birthday',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'MontSerrat',
              ),
            ),
            subtitle: Text(
              _profileData.birthdate,
              style: TextStyle(
                fontSize: 18.0,
                fontFamily: 'MontSerrat',
              ),
            ),
          ),
        ],
      ),
    ]);
  }
}

// Allow us to create a form with defining lines
class SlashClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();

    path.moveTo(0.0, size.height);

    path.lineTo(0.0, size.height / 3.2);
    path.lineTo(size.width + 500, 0.0);
    path.lineTo(size.width, size.height);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
