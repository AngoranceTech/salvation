import 'package:flutter/material.dart';

// Theme of the app
ThemeData salvationTheme = new ThemeData(
  primaryColor: new Color(0xFF439187),
  primaryColorDark: new Color(0xFF05635A),
  primaryColorLight: new Color(0xFF74c2b7),
  primaryColorBrightness: Brightness.light,
  accentColor: new Color(0xFF5F64C0),
  accentColorBrightness: Brightness.dark,
);
