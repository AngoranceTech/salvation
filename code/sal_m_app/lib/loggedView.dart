import 'package:flutter/material.dart';
import 'package:sal_m_app/dashboard.dart';
import 'package:sal_m_app/profile.dart';
import 'package:sal_m_app/wallet.dart';
import 'package:sal_m_app/transferMoney.dart';
import 'package:sal_m_app/parking.dart';
import 'package:sal_m_app/launchView.dart';
import 'package:sal_m_app/menu.dart';
import 'package:sal_m_app/themes/mainTheme.dart';

class LoggedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String appTitle = 'Salvation';

    return WillPopScope(
        onWillPop: () => _logoutAlert(context),
        child: MaterialApp(
          title: appTitle,
          theme: salvationTheme,
          home: Dashboard(),
          routes: {
            '/dashboard': (context) => Dashboard(),
            '/profile': (context) => Profile(),
            '/wallet': (context) => Wallet(),
            '/transferMoney': (context) => TransferMoney(),
            '/parking': (context) => Parking(),
            '/login': (context) => LaunchView(),
          },
        ));
  }
}

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String pageTitle = 'Dashboard';

    return Scaffold(
      appBar: AppBar(
        title: Text(pageTitle),
        elevation: 0.0,
      ),
      body: DashboardPage(),
      drawer: Menu(),
    );
  }
}

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ProfilePage(),
      drawer: Menu(),
    );
  }
}

class Wallet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String pageTitle = 'Wallet';

    return Scaffold(
      appBar: AppBar(
        title: Text(pageTitle),
      ),
      body: WalletPage(),
      drawer: Menu(),
    );
  }
}

class TransferMoney extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String pageTitle = 'Transfer';

    return Scaffold(
      appBar: AppBar(
        title: Text(pageTitle),
      ),
      body: TransferMoneyPage(),
      drawer: Menu(),
    );
  }
}

class Parking extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String pageTitle = 'Parking';

    return Scaffold(
      appBar: AppBar(
        title: Text(pageTitle),
      ),
      body: ParkingPage(),
      drawer: Menu(),
    );
  }
}

// Based on code found on https://codingwithjoe.com/flutter-navigation-how-to-prevent-navigation/
// Prompts a user when pressing the back arrow key to be sure he wants to logout
Future<bool> _logoutAlert(BuildContext context) {
  return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text('Do you want to logout?'),
          content: new Text(
              'If you continue, you will need to login again to use our services.'),
          actions: <Widget>[
            new FlatButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text('No'),
            ),
            new FlatButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: new Text('Yes'),
            ),
          ],
        ),
      ) ??
      false;
}
