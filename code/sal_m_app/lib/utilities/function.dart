import 'package:http/http.dart' as HTTP;
import 'dart:convert';
import 'dart:async';
import 'package:sal_m_app/persistentData/walletData.dart';
import 'package:sal_m_app/persistentData/profileData.dart';

/// Get data profil using profil service API
Future<Map> getDataProfil(int id, String token) async {
  final String _url =
      'https://salvation.angorance.tech/api/prof/profiles/' + id.toString();

  final response = await HTTP.get(_url, headers: {'Authorization': token});

  bool statusOK = (response.statusCode == 200);

  Map data;

  if (statusOK) {
    data = jsonDecode(response.body);
  } else {
    data = new Map();
  }

  data.putIfAbsent('status', () => response.statusCode);

  if (statusOK) {
    ProfileData.instance.setFromJson(data);
  }

  return data;
}

/// Get data wallet using wallet service API
Future<Map> getDataWallet(String token) async {
  final String _url = 'https://salvation.angorance.tech/api/wallet/wallets/';

  final response = await HTTP.get(_url, headers: {'Authorization': token});

  bool statusOK = (response.statusCode == 200);

  Map result = new Map();
  List data;

  if (statusOK) {
    data = jsonDecode(response.body);
  } else {
    result = new Map();
  }

  result.putIfAbsent('status', () => response.statusCode);

  if (statusOK) {
    WalletData.instance.setFromJson(data);
  }

  return result;
}

/// Get data transactions using wallet service API
Future<Map> getDataTransactions(String token) async {
  final String _url =
      'https://salvation.angorance.tech/api/wallet/wallets/${WalletData.instance.id}/transactions?pageNumber=1';

  print(_url);

  final response = await HTTP.get(_url, headers: {'Authorization': token});

  print(response.body);

  bool statusOK = (response.statusCode == 200);

  Map result = new Map();
  List data;

  if (statusOK) {
    data = jsonDecode(response.body);
  } else {
    result = new Map();
  }

  result.putIfAbsent('status', () => response.statusCode);

  if (statusOK) {
    WalletData.instance.setTransactions(data);
  }

  return result;
}

void clearData() {
  ProfileData.instance.clear();
  WalletData.instance.clear();
}
