import 'package:http/http.dart' as HTTP;
import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sal_m_app/interfaces/transfer.dart';
import 'package:sal_m_app/utilities/checkEmail.dart';
import 'package:sal_m_app/persistentData/profileData.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransferMoneyPage extends StatefulWidget {
  @override
  _TransferMoneyPageState createState() => new _TransferMoneyPageState();
}

class _TransferMoneyPageState extends State<TransferMoneyPage> {
  final Transfer _transfer = new Transfer();

  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Form(
        key: _formKey,
        child: new Container(
          padding: const EdgeInsets.symmetric(horizontal: 62.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new TextFormField(
                decoration: new InputDecoration(
                  hintText: 'Enter the email',
                  icon: Icon(Icons.person),
                ),
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                validator: (val) =>
                    isValidEmail(val) ? null : 'Please enter some text',
                onSaved: (val) => _transfer.email = val,
              ),
              new TextFormField(
                decoration: new InputDecoration(
                  hintText: 'Enter the amount (CHF)',
                  icon: Icon(Icons.attach_money),
                ),
                keyboardType: TextInputType.number,
                validator: (val) {
                  var tmp = double.tryParse(val);
                  return tmp == null ? 'Please enter some text' : null;
                },
                onSaved: (val) => _transfer.amount = double.parse(val),
              ),
              new TextFormField(
                decoration: new InputDecoration(
                  hintText: 'Description',
                  icon: Icon(Icons.description),
                ),
                onSaved: (val) => _transfer.description = val,
              ),
              new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: new RaisedButton(
                  color: Theme.of(context).accentColor,
                  onPressed: () {
                    // Validate will return true if the form is valid, or false if
                    // the form is invalid.
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();

                      tryTransfer(_transfer.email, _transfer.amount,
                              _transfer.description)
                          .then((result) {
                        print(result);

                        if (result['status'] == 200) {
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Money transfered successfully!')));

                          _formKey.currentState.reset();
                        } else if (result['status'] == 403) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('Not enough money...')));
                        } else {
                          print(result['status']);
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(
                                  'A problem occured, try again later. Sorry')));
                        }
                      });
                    }
                  },
                  child: new Text('Submit',
                      style: new TextStyle(color: Colors.white)),
                ),
              ),
            ],
          ),
        ));
  }
}

/// Check credentials using authentication service API
Future<Map> tryTransfer(String email, double amount, String description) async {
  final String _url =
      'https://salvation.angorance.tech/api/wallet/transactions';

  ProfileData _profile = ProfileData.instance;

  final _prefs = await SharedPreferences.getInstance();

  String token = _prefs.getString('token');
  int salvationId = _prefs.getInt('salvationId');

  final Map _from = {
    "salvationId": salvationId,
    "name": _profile.firstName + " " + _profile.lastName,
  };

  final Map _to = {
    "email": email,
  };

  final Map _tr = {
    "amount": amount,
    "from": _from,
    "to": _to,
    "description": description,
    "type": "main",
  };

  final String _body = json.encode(_tr);

  final _response =
      await HTTP.post(_url, headers: {'Authorization': token}, body: _body);

  Map data = new Map();

  if (_response.statusCode != 200) {
    data.putIfAbsent('error', () => _response.body);
  }

  data.putIfAbsent("status", () => _response.statusCode);

  return data;
}
