import 'package:flutter/material.dart';
import 'package:sal_m_app/login.dart';
import 'package:sal_m_app/loggedView.dart';
import 'package:sal_m_app/themes/mainTheme.dart';

class LaunchView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String appTitle = 'Salvation';

    return MaterialApp(
      title: appTitle,
      theme: salvationTheme,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: LoginForm(),
      ),
      routes: {
        '/logged': (context) => LoggedView(),
      },
    );
  }
}
