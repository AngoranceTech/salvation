import 'package:http/http.dart' as HTTP;
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sal_m_app/interfaces/parkingPayment.dart';
import 'package:sal_m_app/persistentData/profileData.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ParkingPage extends StatefulWidget {
  @override
  _ParkingPageState createState() => new _ParkingPageState();
}

class _ParkingPageState extends State<ParkingPage> {
  final ParkingPayment _payment = new ParkingPayment();
  final _platenumber = ProfileData.instance.numberplate;

  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Form(
        key: _formKey,
        child: new Container(
          padding: const EdgeInsets.symmetric(horizontal: 62.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new TextFormField(
                initialValue: _platenumber.isEmpty ? '' : _platenumber,
                decoration: new InputDecoration(
                  hintText: 'Enter car plate number',
                  icon: Icon(Icons.directions_car),
                ),
                autocorrect: false,
                keyboardType: TextInputType.text,
                validator: (val) => val.isEmpty ? 'Enter a plate number' : null,
                onSaved: (val) => _payment.numberplate = val,
              ),
              new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: new RaisedButton(
                  color: Theme.of(context).accentColor,
                  onPressed: () {
                    // Validate will return true if the form is valid, or false if
                    // the form is invalid.
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();

                      tryPayment(_payment.numberplate).then((result) {
                        print(result);

                        if (result['status'] == 200) {
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Parking paid successfully!')));

                          _formKey.currentState.reset();
                        } else if (result['status'] == 403) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('Not enough money...')));
                        } else {
                          print(result['status']);
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(
                                  'A problem occured, try again later. Sorry')));
                        }
                      });
                    }
                  },
                  child: new Text('Submit',
                      style: new TextStyle(color: Colors.white)),
                ),
              ),
            ],
          ),
        ));
  }
}

/// Check credentials using authentication service API
Future<Map> tryPayment(String numberplate) async {
  final String _url = 'https://salvation.angorance.tech/api/parking/payment';

  final _prefs = await SharedPreferences.getInstance();

  String token = _prefs.getString('token');

  final Map _body = {
    "plate_number": numberplate,
  };

  final _response =
      await HTTP.post(_url, headers: {'Authorization': token}, body: _body);

  Map data = new Map();

  if (_response.statusCode != 200) {
    data.putIfAbsent('error', () => _response.body);
  }

  data.putIfAbsent("status", () => _response.statusCode);

  return data;
}
