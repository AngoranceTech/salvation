import 'package:http/http.dart' as HTTP;
import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sal_m_app/utilities/function.dart';
import 'package:sal_m_app/persistentData/profileData.dart';
import 'package:sal_m_app/interfaces/user.dart';
import 'package:sal_m_app/utilities/checkEmail.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Create a login (Form Widget)
class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() => new LoginFormState();
}

// Create a corresponding State class. This class will hold the data related to
// the form.
class LoginFormState extends State<LoginForm> with TickerProviderStateMixin {
  final User _user = new User();

  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  //For button
  bool _isPressed = false;
  int state = 0;
  double width = double.infinity;
  Animation _animation;
  GlobalKey globalKey = GlobalKey();
  bool _statusProfile = false;
  bool _statusWallet = false;
  bool _statusTransaction = false;
  String _error = '';

  FocusNode focusNode;

  @override
  void initState() {
    super.initState();

    focusNode = FocusNode();

    focusNode.addListener(() {
      setState(() {
        state = 0;
        width = double.infinity;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey we created above
    return new Form(
        key: _formKey,
        child: new Container(
          padding: const EdgeInsets.symmetric(horizontal: 62.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new TextFormField(
                decoration: new InputDecoration(
                  hintText: 'ID',
                  icon: Icon(Icons.person),
                ),
                focusNode: focusNode,
                keyboardType: TextInputType.emailAddress,
                validator: (val) =>
                    isValidEmail(val) ? null : 'Enter an email!',
                onSaved: (val) => _user.email = val,
              ),
              new TextFormField(
                decoration: new InputDecoration(
                  hintText: 'Password',
                  icon: Icon(Icons.vpn_key),
                ),
                obscureText: true,
                validator: (val) =>
                    val.isEmpty ? 'Please enter some text' : null,
                onSaved: (val) => _user.password = val,
              ),
              new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: PhysicalModel(
                  color: Theme.of(context).accentColor,
                  elevation: _isPressed ? 6.0 : 4.0,
                  borderRadius: new BorderRadius.all(
                      Radius.circular(25.0)), // FIXME: not working
                  child: Container(
                    key: globalKey,
                    height: 48.0,
                    width: width,
                    child: RaisedButton(
                      padding: EdgeInsets.all(0.0),
                      child: buildButtonChild(),
                      color: state == 2
                          ? _statusProfile ? Colors.green : Colors.red
                          : Theme.of(context).accentColor,
                      onPressed: () {
                        // Validate will return true if the form is valid, or false if
                        // the form is invalid.
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          tryAuthentication(_user.email, _user.password)
                              .then((result) {
                            print(result);

                            if (result['status'] == 200) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text('Successfully connected! ')));
                              if (state == 0) {
                                String _token = result['jwt'];
                                animationButton();

                                getDataProfil(result['salvationId'], _token)
                                    .then((result) {
                                  _statusProfile = (result['status'] == 200);

                                  if (!_statusProfile) {
                                    _error = _error + ' ' + result['error'];
                                  }
                                });

                                getDataWallet(_token).then((result) {
                                  _statusWallet = (result['status'] == 200);

                                  if (!_statusWallet) {
                                    _error = _error + ' ' + result['error'];
                                  }

                                  getDataTransactions(_token).then((result) {
                                    _statusTransaction =
                                        (result['status'] == 200);

                                    if (!_statusTransaction) {
                                      _error = _error + ' ' + result['error'];
                                    }
                                  });
                                });

                                _formKey.currentState.reset();
                              }
                            } else {
                              Scaffold.of(context).showSnackBar(
                                  SnackBar(content: Text(result['error'])));
                            }
                          });
                        }
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  ///Animation button
  void animationButton() {
    double initialWidth = globalKey.currentContext.size.width;

    var controller =
        AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    _animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {
          //Reduce the size of the button
          width = initialWidth - ((initialWidth - 48.0) * _animation.value);
        });
      });

    //Start the animation
    controller.forward();

    setState(() {
      //Change the state
      state = 1;
    });

    Timer(Duration(milliseconds: 2000), () {
      setState(() {
        //After 2second the state pass to 2
        state = 2;
      });
      if (_statusProfile && _statusWallet && _statusTransaction) {
        Navigator.pushNamed(context, '/logged');
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(_error)));
      }
    });
  }

  ///Different display button
  Widget buildButtonChild() {
    //Initial state, display login text on the button
    if (state == 0) {
      return Text('login',
          style: TextStyle(color: Colors.white, fontSize: 16.0));
      //State 1 , the button is totally reduced, we display the spinner
    } else if (state == 1) {
      return CircularProgressIndicator(
          value: null, valueColor: AlwaysStoppedAnimation<Color>(Colors.white));

      //State 2, we display the check icon
    } else {
      return _statusProfile
          ? Icon(Icons.check, color: Colors.white)
          : Icon(Icons.close, color: Colors.white);
    }
  }
}

/// Check credentials using authentication service API
Future<Map> tryAuthentication(String email, String password) async {
  final String _url =
      'https://salvation.angorance.tech/api/auth/authentication';

  final prefs = await SharedPreferences.getInstance();

  final Map<String, dynamic> _body = {"email": email, "password": password};

  final response = await HTTP.post(_url, body: _body);

  bool statusOK = (response.statusCode == 200);

  Map data;

  if (statusOK) {
    data = jsonDecode(response.body);
  } else {
    data = new Map();
    data.putIfAbsent('error', () => response.body);
  }

  data.putIfAbsent("status", () => response.statusCode);

  prefs.setString('token', statusOK ? data['jwt'] : "");
  prefs.setInt('salvationId', statusOK ? data['salvationId'] : null);
  ProfileData.instance.setToken(statusOK ? data['jwt'] : "");

  return data;
}
