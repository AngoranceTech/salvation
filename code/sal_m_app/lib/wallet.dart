import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sal_m_app/utilities/function.dart';
import 'package:sal_m_app/persistentData/profileData.dart';
import 'package:sal_m_app/persistentData/walletData.dart';

class WalletPage extends StatefulWidget {
  @override
  _WalletPageState createState() => new _WalletPageState();
}

// Based on tutorial: https://www.youtube.com/watch?v=K2cyOZtQvwY
// And tutorial: https://www.youtube.com/watch?v=n2Dav9ONJsY
class _WalletPageState extends State<WalletPage> {
  final WalletData _walletData = WalletData.instance;

  @override
  Widget build(BuildContext context) {
    final _token = ProfileData.instance.token;
    String _amount = _walletData.amount.toString();

    return new Stack(
      children: <Widget>[
        Positioned(
            width: MediaQuery.of(context).size.width,
            top: 50.0,
            child: Column(
              children: <Widget>[
                Text('Wallet',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'MontSerrat',
                    )),
                SizedBox(height: 30.0),
                Card(
                    elevation: 4.0,
                    margin: new EdgeInsets.symmetric(horizontal: 50.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          leading: Icon(Icons.account_balance),
                          trailing: IconButton(
                            icon: Icon(Icons.autorenew),
                            onPressed: () {
                              getDataWallet(_token).then((result) {
                                if (result['status'] == 200) {
                                  this.setState(() {
                                    _amount = _walletData.amount.toString();
                                  });
                                }
                              });
                            },
                          ),
                          title: Text(
                            'Balance',
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'MontSerrat',
                            ),
                          ),
                          subtitle: Text(
                            _amount,
                            style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'MontSerrat',
                            ),
                          ),
                        ),
                      ],
                    )),
                SizedBox(height: 80.0),
                Text('Transactions',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'MontSerrat',
                    )),
                SizedBox(height: 30.0),
                DataTable(
                  columns: <DataColumn>[
                    DataColumn(label: Text('From'), onSort: (i, b) {}),
                    DataColumn(label: Text('To'), onSort: (i, b) {}),
                    DataColumn(
                        label: Text('Amount'),
                        onSort: (i, b) {},
                        numeric: true),
                  ],
                  rows: _walletData.transactions
                      .map((tr) => DataRow(cells: [
                            DataCell(Text(tr.from),
                                showEditIcon: false,
                                placeholder: false,
                                onTap: () => false),
                            DataCell(Text(tr.to),
                                showEditIcon: false,
                                placeholder: false,
                                onTap: () => false),
                            DataCell(Text(tr.amount.toString()),
                                showEditIcon: false,
                                placeholder: false,
                                onTap: () => false),
                          ]))
                      .toList(),
                )
              ],
            )),
      ],
    );
  }
}
