# Salvation - API - Parking service

Authors : Héléna Reymond, Daniel Gonzalez Lopez, Bryan Curchod, François Burgener

This is a document which describes the **Parking** service.

---

## Description

This service is used by the administrator and clients to manage parking transactions on Salvation.

```javascript
// Model of the parking transaction
{
  salvationId: {
    type: 'number',
    unique: true,
    required: true,
  },
  name: {
    type: 'string',
  },
  plate_number: {
    type: 'string',
  },
  date: {
    type: 'string',
  }
}
```

---

## Technologies used

### Backend
- <u>Framework</u>: Sails
- <u>Dependencies</u>: jsonwebtoken, eslint

### MongoDB Database
- <u>Database</u>: parkingDB
- <u>Collections</u>: parking

### API tests
- Mocha
- Chai
- Axios

---

## API

Here are the APIs specifications of our service.
The complete specification is available in `/specs` folder.

### Url of the service
```
https://salvation.angorance.tech/api/parking
```

### Endpoints

```javascript
// Admin
GET    =>      '/paid'                  // List all parking transactions of the day

// Parking
POST   =>      '/payment'               // Create a parking transaction
```

---

## Functional tests

The goal of these `7 tests` is to verify all the endpoints of our service.

### CRUD operations on parking transactions

<img src="parking_tests.PNG">