const { expect } = require('chai');
const sails = require('sails');
const axios = require('axios');
const errorMessage = require('../api/errors/errorMessage');

const client = axios.create({
  baseURL: 'http://localhost:1337',
  timeout: 2000,
});

const authenticationService = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/auth',
  timeout: 2000,
});

describe('PARKING SERVICE', function () {

  /* ========================================================================
  /*  LOGIN (role admin)
  /*====================================================================== */

  let token = "";
  let adminId = -1;

  it('should log in the administrator', function () {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'admin@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        token = response.data.jwt;
        salvationId = response.data.salvationId;
        adminId = salvationId;

        const expected = { jwt: token, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch((error) => {
        console.log(error.data);
        Promise.reject();
      });
  });

  /* ========================================================================
  /*  LOGIN (role service)
  /*====================================================================== */

  let tokenStudent = "";

  it('should log in the cafeteria account', function () {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'cafeteria@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        tokenStudent = response.data.jwt;
        salvationId = response.data.salvationId;

        const expected = { jwt: tokenStudent, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch(error => console.log(error));
  });

  /* ========================================================================
  /*  PARKING TRANSACTION CREATION (several roles)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .post(`/payment`, {})
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should create a parking transaction for admin (role admin)', function () {
    const data = {
      plate_number: "VS 243 678",
    };
    const header = { headers: { Authorization: token } };

    return client
      .post(`/payment`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  it('should allow cafeteria account to pay the parking (role service)', function () {
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .post(`/payment`, {}, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.not.eql(errorMessage.tokenIsMissing);
      });
  });

  /* ========================================================================
  /*  PARKING TRANSACTIONS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should retrieve 1 parking transaction (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get('/paid', header)
      .then((response) => {
        const expected = [
          {
            name: response.data[0].name,
            plate_number: "VS 243 678",
          }
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  PARKING TRANSACTIONS RETRIEVAL (role service)
  /*====================================================================== */

  it('should say that only an administrator can do this (role service)', function () {
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .get('/paid', header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });
});