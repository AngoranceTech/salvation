// ----------------------------------------------------------------
// QUERY HANDLER --------------------------------------------------

const noParkingFound = 'No parking transaction has been found';
const noParkingTransactionCreated = 'No parking transaction has been created';

// ----------------------------------------------------------------
// 400 - BAD REQUEST ----------------------------------------------

const requestInformationMissing = 'Request format not respected';

// ----------------------------------------------------------------
// 401 - UNAUTHORIZED ---------------------------------------------

const tokenIsIncomplete = 'Token not complete';
const tokenIsMissing = 'No token provided';
const tokenIsInvalid = 'The token provided isn\'t valid';

// ----------------------------------------------------------------
// 403 - FORBIDDEN ------------------------------------------------
const notAnAdmin = 'Only an administrator can do this';

module.exports = {
  noParkingFound,
  noParkingTransactionCreated,
  requestInformationMissing,
  tokenIsIncomplete,
  tokenIsMissing,
  tokenIsInvalid,
  notAnAdmin,
};
