/* global Profile */
/* eslint no-console: 0 */

const errorHandler = require('../errors/errorHandler');
const errorMessage = require('../errors/errorMessage');

const db = Parking.getDatastore().manager.collection('parking');

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * Get the parking inventory from the database
 */
function selectParkingInventory(date) {
  return db.find({ date: {$lt: new Date(), $gte: new Date(new Date().setDate(new Date().getDate()-1))}}, { _id: 0, salvationId: 0, date: 0 }).sort({ name: 1 }).toArray()
    .then((inventory) => {
      console.log('Parking inventory retrieved');
      return inventory;
    });
}

// ----------------------------------------------------------------
// Payment OPERATIONS ---------------------------------------------

/**
 * Create one parking transaction in the database
 * @param {*} salvationId User's id
 * @param {*} name User's name
 * @param {*} plate_number User's plate number
 */
function payParking(salvationId, name, plate_number) {

  const date = new Date();

  return db.insertOne({ salvationId, name, plate_number, date })
    .then((operation) => {
      // Parking transaction has been created
      if (operation.insertedCount === 1) {
        console.log(`Parking paid by ${name}`);
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noParkingTransactionCreated);
    });
}

module.exports = {
  selectParkingInventory,
  payParking,
};
