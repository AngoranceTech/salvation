/**
 * ParkingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

/* global sails */
/* eslint no-undef: "error" */
/* eslint no-console: 0 */

const jwt = require('jsonwebtoken');
const queries = require('../queries/queryHandler');
const errorHandler = require('../errors/errorHandler');
const axios = require('axios');

const { secret } = sails.config.parkingConf;

const profile = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/prof',
  timeout: 2000,
});

const wallet = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/wallet',
  timeout: 2000,
});

// ----------------------------------------------------------------
// SEND RESPONSE --------------------------------------------------
function sendResponse(res, result) {
  res.status(200);

  if (result !== undefined) {
    res.json(result);
  } else {
    res.send();
  }
}

// ----------------------------------------------------------------
// SEND SPECIAL ERROR ---------------------------------------------
function sendSpecialError(err, res) {
  if (err instanceof errorHandler.SpecialError) {
    errorHandler.sendError(err.err, res, err.code);
  } else {
    errorHandler.sendError(err, res, 0);
  }
}

// ----------------------------------------------------------------
// AUTHORIZATION VALIDATION ---------------------------------------
function validAuthorization(authorization, res) {
  if (authorization !== undefined) {
    try {
      const token = jwt.verify(authorization, secret);

      if (token.salvationId !== undefined && token.role !== undefined) {
        return token;
      }
      errorHandler.tokenIsIncomplete(res);
      return undefined;

    } catch (err) {
      errorHandler.tokenIsInvalid(res);
      return undefined;
    }
  }

  errorHandler.tokenIsMissing(res);
  return undefined;
}

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * [GET]
 * Get the list of parking transactions of the day
 *
 * @param {*} req body(plateNumber), header(authorization)
 * @param {*} res json(parkingInventory) and code 200, an error otherwise
 */
function getParkingInventory(req, res) {
  console.log('GET_PARKING_INVENTORY: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  if (token !== undefined) {
    // Only the administrator can do this operation
    if (token.role === 'admin') {
      // Get the parking inventory from the database
      queries.selectParkingInventory(new Date())
        .then(result => sendResponse(res, result))
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.notAnAdmin(res);
    }
  }
}

// ----------------------------------------------------------------
// Payment OPERATIONS ---------------------------------------------

/**
 * [POST]
 * Pay the parking
 *
 * @param {*} req header(authorization), body(plateNumber)
 * @param {*} res code 201, an error otherwise
 */
function payParking(req, res) {
  console.log('PAY_PARKING: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const plateNumber = req.body.plate_number;

  if (token !== undefined) {
    if (plateNumber !== undefined) {
      // Get the name of the user
      profile.get(`/nameById/${token.salvationId}`)
        .then((response) => {
          console.log(`Name ${response.data} retrieved`);

          const name = response.data;
          const data = {
            'amount': 6,
            'from': {
              'salvationId': token.salvationId,
              'name': name
            },
            'to': {
              'email': 'parking@salvation.ch'
            },
            'type': 'main',
            'description': 'Parking payment'
          };

          // Create the transaction
          const header = { headers: { Authorization: authorization } };
          wallet.post('/transactions', data, header)
            .then(() => {
              console.log(`Transaction created for ${name}`);

              // Create the parking transaction in the database
              queries.payParking(token.salvationId, name, plateNumber)
                .then(result => sendResponse(res, result))
                .catch(err => sendSpecialError(err, res));
            })
            .catch(err => sendSpecialError(err, res));
        })
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.requestInformationMissing(res);
    }
  }
}

module.exports = {
  getParkingInventory,
  payParking,
};

