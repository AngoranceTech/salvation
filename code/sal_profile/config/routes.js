/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /**
   * Services
   */
  'GET      /nameById/:userId': 'ProfileController.getNameById',

  /**
   * Admin
   * 
   * Entrypoints for administration. Allows to create profiles and list them all. 
   * Allow to delete them too.
   */
  'GET       /profiles': 'ProfileController.getProfiles',
  'POST      /profiles/:userId': 'ProfileController.createProfile',
  'PATCH     /profiles/:userId': 'ProfileController.updateProfile',
  'DELETE    /profiles/:userId': 'ProfileController.removeProfile',

  /**
   * Profile
   * 
   * Allows users to access and modify their profile.
   */
  'GET       /profiles/:userId': 'ProfileController.getProfile',
};
