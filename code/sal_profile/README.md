# Salvation - API - Profile service

Authors : Héléna Reymond, Daniel Gonzalez Lopez, Bryan Curchod, François Burgener

This is a document which describes the **Profile** service.

---

## Description

This service is used by the administrator and clients to manage profiles on Salvation.

```javascript
// Model of the user's profile
{
  salvationId: {
    type: 'number',
    unique: true,
    required: true,
  },
  first_name: {
    type: 'string',
  },
  last_name: {
    type: 'string',
  },
  birth_date: {
    type: 'string',
  },
  address: {
    type: 'string',
  },
  npa: {
    type: 'number',
  },
  city: {
    type: 'string',
  },
  phone_number: {
    type: 'string',
  },
  email: {
    type: 'string',
  },
  plate_number: {
    type: 'string',
  }
}
```

---

## Technologies used

### Backend
- <u>Framework</u>: Sails
- <u>Dependencies</u>: jsonwebtoken, eslint

### MongoDB Database
- <u>Database</u>: profileDB
- <u>Collections</u>: profile

### API tests
- Mocha
- Chai
- Axios

---

## API

Here are the APIs specifications of our service.
The complete specification is available in `/specs` folder.

### Url of the service
```
https://salvation.angorance.tech/api/prof
```

### Endpoints

```javascript
// Admin
POST   =>      '/profiles/{userId}'     // Create a profile
GET    =>      '/profiles'              // List all profiles
PATCH  =>      '/profiles/{userId}'     // Modify a profile
DELETE =>      '/profiles/{userId}'     // Delete a profile

// Profiles
GET    =>      '/nameById/{userId}'     // Retrieve the user's name by ID
GET    =>      '/profiles/{userId}'     // Retrieve a profile
```

---

## Functional tests

The goal of these `22 tests` is to verify all the endpoints of our service.

### CRUD operations with admin role

<img src="crud_tests.PNG">

### CRUD operations with student role

<img src="student_tests.PNG">