/**
 * Profile.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    salvationId: {
      type: 'number',
      unique: true,
      required: true,
    },
    first_name: {
      type: 'string',
    },
    last_name: {
      type: 'string',
    },
    birth_date: {
      type: 'string',
    },
    address: {
      type: 'string',
    },
    npa: {
      type: 'number',
    },
    city: {
      type: 'string',
    },
    phone_number: {
      type: 'string',
    },
    email: {
      type: 'string',
    },
    plate_number: {
      type: 'string',
    },
  },
};
