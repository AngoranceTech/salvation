// ----------------------------------------------------------------
// QUERY HANDLER --------------------------------------------------
const nonExistentProfile = 'This user doesn\'t exist';
const noProfileCreated = 'No user has been created';
const noProfileUpdated = 'No user has been updated';
const noProfileFound = 'No user has been found';
const noProfileRemoved = 'No user has been removed';

// ----------------------------------------------------------------
// 400 - BAD REQUEST ----------------------------------------------

const userIdMissingInUrl = 'No user ID provided in the url';
const requestInformationMissing = 'Request format not respected';

// ----------------------------------------------------------------
// 401 - UNAUTHORIZED ---------------------------------------------

const tokenIsIncomplete = 'Token not complete';
const tokenIsMissing = 'No token provided';
const tokenIsInvalid = 'The token provided isn\'t valid';

// ----------------------------------------------------------------
// 403 - FORBIDDEN ------------------------------------------------
const notAnAdmin = 'Only an administrator can do this';

module.exports = {
  nonExistentProfile,
  noProfileCreated,
  noProfileUpdated,
  noProfileFound,
  noProfileRemoved,
  userIdMissingInUrl,
  requestInformationMissing,
  tokenIsIncomplete,
  tokenIsMissing,
  tokenIsInvalid,
  notAnAdmin,
};
