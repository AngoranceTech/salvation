/**
 * ProfileController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

/* global sails */
/* eslint no-undef: "error" */
/* eslint no-console: 0 */

const jwt = require('jsonwebtoken');
const queries = require('../queries/queryHandler');
const errorHandler = require('../errors/errorHandler');

const { secret } = sails.config.profileConf;

// ----------------------------------------------------------------
// SEND RESPONSE --------------------------------------------------
function sendResponse(res, result) {
  res.status(200);

  if (result !== undefined) {
    res.json(result);
  } else {
    res.send();
  }
}

// ----------------------------------------------------------------
// SEND SPECIAL ERROR ---------------------------------------------
function sendSpecialError(err, res) {
  if (err instanceof errorHandler.SpecialError) {
    errorHandler.sendError(err.err, res, err.code);
  } else {
    errorHandler.sendError(err, res, 0);
  }
}

// ----------------------------------------------------------------
// AUTHORIZATION VALIDATION ---------------------------------------
function validAuthorization(authorization, res) {
  if (authorization !== undefined) {
    try {
      const token = jwt.verify(authorization, secret);

      if (token.salvationId !== undefined && token.role !== undefined) {
        return token;
      }
      errorHandler.tokenIsIncomplete(res);
      return undefined;

    } catch (err) {
      errorHandler.tokenIsInvalid(res);
      return undefined;
    }
  }

  errorHandler.tokenIsMissing(res);
  return undefined;
}

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * [GET]
 * Get all profiles
 *
 * @param {*} req header(authorization)
 * @param {*} res json([profiles]) and code 200, an error otherwise
 */
function getProfiles(req, res) {
  console.log('LIST_PROFILES: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  if (token !== undefined) {
    // Only the administrator can do this operation
    if (token.role === 'admin') {
      // Get all profiles from the database
      queries.selectProfiles()
        .then(result => sendResponse(res, result))
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.notAnAdmin(res);
    }
  }
}

/**
 * [POST]
 * Create a profile
 *
 * @param {*} req params(userId), header(authorization), body(profile)
 * @param {*} res code 200, an error otherwise
 */
function createProfile(req, res) {
  console.log('CREATE_PROFILE: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const profile = req.body;

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      profile.salvationId = userId;

      // Only the administrator can do this operation
      if (token.role === 'admin') {
        // Create the profile in the database
        queries.insertProfile(profile)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

/**
 * [PATCH]
 * Update a profile
 *
 * @param {*} req params(userId), header(authorization), body(profile)
 * @param {*} res code 200, an error otherwise
 */
function updateProfile(req, res) {
  console.log('UPDATE_PROFILE: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const profile = req.body;

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Only the administrator can do this operation
      if (token.role === 'admin') {
        // Update the profile in the database
        queries.updateProfile(userId, profile)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

/**
 * [DELETE]
 * Remove a profile
 *
 * @param {*} req params(userId), header(authorization)
 * @param {*} res code 200, an error otherwise
 */
function removeProfile(req, res) {
  console.log('REMOVE_PROFILE: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Only the administrator can do this operation
      if (token.role === 'admin') {
        // Remove the profile from the database
        queries.deleteProfile(userId)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

// ----------------------------------------------------------------
// Profile OPERATIONS ---------------------------------------------

/**
 * [GET]
 * Get a profile with an ID
 *
 * @param {*} req params(userId), header(authorization)
 * @param {*} res json(profile) and code 200, an error otherwise
 */
function getProfile(req, res) {
  console.log('GET_PROFILE: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Only the administrator or the concerned user can do this operation
      if (token.role === 'admin' || token.salvationId === userId) {
        // Get the profile from the database
        queries.selectProfile(userId)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

function getNameById(req, res) {
  console.log('GET_NAME_BY_ID: ');

  // Check information
  const userId = parseInt(req.params.userId);

  if (userId !== undefined) {
    // Get name from the database
    queries.getNameById(userId)
      .then(result => sendResponse(res, result))
      .catch(err => sendSpecialError(err, res));

  } else {
    errorHandler.userIdMissingInUrl(res);
  }

}

module.exports = {
  getProfiles,
  getProfile,
  createProfile,
  updateProfile,
  removeProfile,
  getNameById,
};
