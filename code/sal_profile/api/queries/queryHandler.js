/* global Profile */
/* eslint no-console: 0 */

const errorHandler = require('../errors/errorHandler');
const errorMessage = require('../errors/errorMessage');

const db = Profile.getDatastore().manager.collection('profile');


// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * Retrieve the name linked to a specific ID
 * @param {*} userId Id of the name to retrieve
 */
function getNameById(userId) {
  return db.findOne({ salvationId: userId }, { _id: 0, first_name: 1, last_name: 1 })
    .then((name) => {
      // Name has been found
      if (name != null) {
        let fullName = name.first_name + (name.last_name !== undefined ? ' ' + name.last_name : '');
        console.log('Name retrieved');
        console.log(`ID: ${userId}, type: ${typeof userId}, name: ${JSON.stringify(fullName)}`);
        return fullName;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noProfileFound);
    });
}

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * Get all profiles from the database
 */
function selectProfiles() {
  return db.find({}, { _id: 0 }).sort({ salvationId: 1 }).toArray()
    .then((profiles) => {
      console.log('Profiles retrieved');
      return profiles;
    });
}

/**
 * Create one profile in the database
 * @param {*} profile Profile to create
 */
function insertProfile(profile = {}) {
  return db.insertOne(profile)
    .then((operation) => {
      // Profile has been created
      if (operation.insertedCount === 1) {
        console.log('Profile created');
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noProfileCreated);
    });
}

/**
 * Update one profile in the database
 * @param {*} id Id of the profile to update
 * @param {*} profile Profile to update
 */
function updateProfile(id, profile = {}) {
  return db.updateOne({ salvationId: id }, { $set: profile })
    .then((operation) => {
      // Profile has been updated
      if (operation.result.n === 1) {
        console.log('Profile updated');
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noProfileUpdated);
    });
}

/**
 * Delete one profile from the database
 * @param {*} id Id of the profile to delete
 */
function deleteProfile(id) {
  return db.removeOne({ salvationId: id })
    .then((operation) => {
      // Profile has been removed
      if (operation.result.n === 1) {
        console.log('Profile removed');
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noProfileRemoved);
    });
}

// ----------------------------------------------------------------
// Profile OPERATIONS ---------------------------------------------

/**
 * Get one profile from the database
 * @param {*} id Id of the user
 */
function selectProfile(id) {
  return db.findOne({ salvationId: id }, { _id: 0, salvationId: 0 })
    .then((profile) => {
      // Profile has been found
      if (profile != null) {
        console.log('Profile retrieved');
        return profile;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noProfileFound);
    });
}

module.exports = {
  selectProfiles,
  selectProfile,
  insertProfile,
  updateProfile,
  deleteProfile,
  getNameById,
};
