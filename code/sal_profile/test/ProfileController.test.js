const { expect } = require('chai');
const sails = require('sails');
const axios = require('axios');
const errorMessage = require('../api/errors/errorMessage');

const client = axios.create({
  baseURL: 'http://localhost:1337',
  timeout: 2000,
});

const authenticationService = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/auth',
  timeout: 2000,
});

describe('PROFILE SERVICE', function () {

  /* ========================================================================
  /*  LOGIN (role admin)
  /*====================================================================== */

  let token = "";
  let adminId = -1;

  it('should log in the administrator', function () {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'admin@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        token = response.data.jwt;
        salvationId = response.data.salvationId;
        adminId = salvationId;

        const expected = { jwt: token, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch((error) => {
        console.log(error.data);
        Promise.reject();
      });
  });

  /* ========================================================================
  /*  LOGIN (role student)
  /*====================================================================== */

  let tokenStudent = "";
  let userId = -1;

  it('should log in the student test', function () {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'test@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        tokenStudent = response.data.jwt;
        salvationId = response.data.salvationId;
        userId = salvationId;

        const expected = { jwt: tokenStudent, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch(error => console.log(error));
  });

  /* ========================================================================
  /*  PROFILES RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get('/profiles')
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve a list of 4 profiles (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get('/profiles', header)
      .then((response) => {
        const expected = [
          {"email": "admin@salvation.ch", "first_name": "Admin", "salvationId": 0},
          {"email": "cafeteria@salvation.ch", "first_name": "Cafeteria", "salvationId": 2},
          {"email": "parking@salvation.ch", "first_name": "Parking", "salvationId": 3},
          {"email": "printer@salvation.ch", "first_name": "Printer", "salvationId": 4}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  PROFILE CREATION (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .post(`/profiles/${userId}`, {})
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should create a profile for user test (role admin)', function () {
    const data = {
      first_name: "MonPrenom",
      last_name: "MonNom",
      birth_date: '1993.12.05',
      address: "Rte de chez moi",
      npa: 1869,
      city: "MonChezMoi",
      phone_number: "MonTelephone",
      email: "test@salvation.ch",
      plate_number: "VS 243 678",
    };
    const header = { headers: { Authorization: token } };

    return client
      .post(`/profiles/${userId}`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  STUDENT'S NAME RETRIEVAL BY ID
  /*====================================================================== */

  it('should retrieve the name of the student\'s ID', function () {

    return client
      .get(`/nameById/${userId}`)
      .then((response) => {
        const expected = 'MonPrenom MonNom';
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  ADMIN'S NAME RETRIEVAL BY ID
  /*====================================================================== */

  it('should retrieve the name of the admin\'s ID', function () {

    return client
      .get(`/nameById/${adminId}`)
      .then((response) => {
        const expected = 'Admin';
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  PROFILES RETRIEVAL (role admin)
  /*====================================================================== */

  it('should retrieve 5 profiles (1 admin, 3 services, 1 student) (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get('/profiles', header)
      .then((response) => {
        const expected = [
          {"email": "admin@salvation.ch", "first_name": "Admin", "salvationId": 0},
          {
            salvationId: userId,
            first_name: "MonPrenom",
            last_name: "MonNom",
            birth_date: '1993.12.05',
            address: "Rte de chez moi",
            npa: 1869,
            city: "MonChezMoi",
            phone_number: "MonTelephone",
            email: "test@salvation.ch",
            plate_number: "VS 243 678",
          },
          {"email": "cafeteria@salvation.ch", "first_name": "Cafeteria", "salvationId": 2},
          {"email": "parking@salvation.ch", "first_name": "Parking", "salvationId": 3},
          {"email": "printer@salvation.ch", "first_name": "Printer", "salvationId": 4}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  PROFILE UPDATE (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    const data = { email: 'testModified@salvation.ch' };

    return client
      .patch(`/profiles/${userId}`, data)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should update a profile (role admin)', function () {
    const data = { email: 'testModified@salvation.ch' };
    const header = { headers: { Authorization: token } };

    return client
      .patch(`/profiles/${userId}`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  PROFILE RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get(`/profiles/${userId}`)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve the updated profile from the list of profiles (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/profiles/${userId}`, header)
      .then((response) => {
        const expected = {
          first_name: "MonPrenom",
          last_name: "MonNom",
          birth_date: '1993.12.05',
          address: "Rte de chez moi",
          npa: 1869,
          city: "MonChezMoi",
          phone_number: "MonTelephone",
          email: "testModified@salvation.ch",
          plate_number: "VS 243 678",
        };

        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  PROFILE RETRIEVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .get(`/profiles/${userId + 1}`, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  it('should retrieve the user\'s profile from the list of profiles (role student)', function () {
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .get(`/profiles/${userId}`, header)
      .then((response) => {
        const expected = {
          first_name: "MonPrenom",
          last_name: "MonNom",
          birth_date: '1993.12.05',
          address: "Rte de chez moi",
          npa: 1869,
          city: "MonChezMoi",
          phone_number: "MonTelephone",
          email: "testModified@salvation.ch",
          plate_number: "VS 243 678",
        };

        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  PROFILES RETRIEVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .get('/profiles', header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  PROFILE CREATION (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const data = {
      first_name: "MonPrenom",
      last_name: "MonNom",
      birth_date: '1993.12.05',
      address: "Rte de chez moi",
      npa: 1869,
      city: "MonChezMoi",
      phone_number: "MonTelephone",
      email: "test2@salvation.ch",
      plate_number: "VS 243 678",
    };
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .post(`/profiles/${userId + 1}`, data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  PROFILE UPDATE (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const data = { email: 'testModifiedAgain@salvation.ch' };
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .patch(`/profiles/${userId}`, data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  PROFILE REMOVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenStudent } };

    return client
      .delete(`/profiles/${userId}`, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  PROFILE REMOVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .delete(`/profiles/${userId}`)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should remove test\'s profile (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .delete(`/profiles/${userId}`, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  PROFILES RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the profile doesn\'t exist (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/profiles/${userId}`, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.noProfileFound);
      });
  });
});