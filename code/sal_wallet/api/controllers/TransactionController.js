const jwt = require('jsonwebtoken');
const queries = require('../queries/queryHandler');
const errorHandler = require('../errors/errorHandler');
const axios = require('axios');

const { secret } = sails.config.walletConf;

const authentication = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/auth',
  timeout: 2000,
});

const profile = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/prof',
  timeout: 2000,
});

// ----------------------------------------------------------------
// SEND RESPONSE --------------------------------------------------
function sendResponse(res, result) {
  res.status(200);

  if (result !== undefined) {
    res.json(result);
  } else {
    res.send();
  }
}

// ----------------------------------------------------------------
// SEND SPECIAL ERROR ---------------------------------------------
function sendSpecialError(err, res) {
  if (err instanceof errorHandler.SpecialError) {
    errorHandler.sendError(err.err, res, err.code);
  } else {
    errorHandler.sendError(err, res, 500);
  }
}

// ----------------------------------------------------------------
// AUTHORIZATION VALIDATION ---------------------------------------
function validAuthorization(authorization, res) {
  if (authorization !== undefined) {
    try {
      const token = jwt.verify(authorization, secret);

      if (token.salvationId !== undefined && token.role !== undefined) {
        return token;
      }
      errorHandler.tokenIsIncomplete(res);
      return undefined;

    } catch (err) {
      errorHandler.tokenIsInvalid(res);
      return undefined;
    }
  }

  errorHandler.tokenIsMissing(res);
  return undefined;
}

// ----------------------------------------------------------------
// VALIDATE TRANSACTION BODY --------------------------------------

function validateTx(transaction) {
  return (transaction.amount !== undefined) && (transaction.to !== undefined)
    && (transaction.from !== undefined) && (transaction.type !== undefined)
    && (transaction.from.salvationId !== undefined) && (transaction.from.name !== undefined)
    && (transaction.to.salvationId !== undefined || transaction.to.email !== undefined) 
    && (transaction.description !== undefined);
}

// ----------------------------------------------------------------
// User OPERATIONS -----------------------------------------------

/**
 * [POST]
 * Create new transaction concerning two entities (from and to)
 *
 * @param {*} req header(authorization), body(TODO)
 * @param {*} res json(TODO) and code 200, an error otherwise
 */
async function createTransaction(req, res) {
  console.log('CREATE_TRANSACTION: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const transaction = req.body;

  // Check information
  if (token !== undefined) {
    if (validateTx(transaction)) {

      // Retrieve salvationId for to part if not given
      if(transaction.to.salvationId === undefined && transaction.to.email !== undefined) {
        let request = '/idByEmail?email=' + transaction.to.email;
        transaction.to.salvationId = await authentication.get(request).then(response => response.data);
      }

      // Create transaction
      let r = '/nameById/' + transaction.to.salvationId;

      profile.get(r)
        .then((response) => {
          transaction.to.name = response.data;
          transaction.date = new Date().toString();

          console.log(transaction);

          queries.enoughMoney(transaction)
            .then(needed => {
              if (needed > 0) {
                errorHandler.notEnoughMoney(res, needed);
              } else {
                queries.createTransaction(transaction)
                  .then(result => sendResponse(res, result))
                  .catch(err => sendSpecialError(err, res));
              }
            });
        });
    } else {
      errorHandler.requestInformationMissing(res);
    }
  }
}

/**
 * [GET]
 * Get list of transactions 
 * @param {*} req 
 * @param {*} res 
 */
function getTransactionsByWallet(req, res) {
  console.log('GET_TRANSACTIONS: ');

  let pageNumber = req.query.pageNumber;
  let walletId = req.params.walletId;

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Check information
  if (token !== undefined) {
    if (pageNumber !== undefined && walletId !== undefined) {
      queries.getWalletById(walletId)
        .then(wallet => {
          if (token.salvationId === wallet.salvationId || token.role === 'admin') {
            queries.getTransactionsByWalletId(walletId, pageNumber)
              .then(result => sendResponse(res, result))
              .catch(err => sendSpecialError(err, res));
          } else {
            errorHandler.notAnAdmin(res);
          }
        })
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.requestInformationMissing(res);
    }

  }
}

module.exports = {
  createTransaction,
  getTransactionsByWallet,
};
