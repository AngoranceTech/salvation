const jwt = require('jsonwebtoken');
const queries = require('../queries/queryHandler');
const errorHandler = require('../errors/errorHandler');

const { secret } = sails.config.walletConf;

// ----------------------------------------------------------------
// SEND RESPONSE --------------------------------------------------
function sendResponse(res, result) {
  res.status(200);

  if (result !== undefined) {
    res.json(result);
  } else {
    res.send();
  }
}

// ----------------------------------------------------------------
// SEND SPECIAL ERROR ---------------------------------------------
function sendSpecialError(err, res) {
  if (err instanceof errorHandler.SpecialError) {
    errorHandler.sendError(err.err, res, err.code);
  } else {
    errorHandler.sendError(err, res, 0);
  }
}

// ----------------------------------------------------------------
// AUTHORIZATION VALIDATION ---------------------------------------
function validAuthorization(authorization, res) {
  if (authorization !== undefined) {
    try {
      const token = jwt.verify(authorization, secret);

      if (token.salvationId !== undefined && token.role !== undefined) {
        return token;
      }
      errorHandler.tokenIsIncomplete(res);
      return undefined;

    } catch (err) {
      errorHandler.tokenIsInvalid(res);
      return undefined;
    }
  }

  errorHandler.tokenIsMissing(res);
  return undefined;
}

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * [GET]
 * Get user's wallets
 *
 * @param {*} req headers(authorization), params(userId)
 * @param {*} res json(CODE) and code 200 if ok, an error otherwise
 */
function getUserWallets(req, res) {
  console.log('GET_USERS_WALLETS(ADMIN): ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  const userId = parseInt(req.params.userId);

  // Check information
  if (token !== undefined) {
    // Only the administrator can do this operation
    if (token.role === 'admin') {
      // Get the wallets from the database
      queries.getUserWallets(userId)
        .then(result => sendResponse(res, result))
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.notAnAdmin(res);
    }
  }
}

/**
 * [POST]
 * Create user's wallets (print and main)
 *
 * @param {*} req params(userId), header(authorization), body(TODO)
 * @param {*} res json(TODO) and code 200, an error otherwise
 */
function createWallets(req, res) {
  console.log('CREATE_USERS_WALLETS: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const wallets = req.body;
  const userId = parseInt(req.params.userId, 10);

  // Check information
  if (token !== undefined) {
    // Only the administrator can do this operation
    if (token.role === 'admin') {

      let errorOccured = false;

      for (let k in wallets) {
        let wallet = wallets[k];
        wallet.salvationId = userId;

        if (wallet.amount === undefined || wallet.type === undefined) { 
          errorOccured = true; 
          errorHandler.requestInformationMissing(res);
          break;
        }
      }

      if (!errorOccured) {     
        // Create the user in the database
        queries.createWallet(wallets)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      }
    } else {
      errorHandler.notAnAdmin(res);
    }
  }
}

/**
 * [DELETE]
 * Remove all user's wallets
 *
 * @param {*} req params(userId), header(authorization)
 * @param {*} res code 200, an error otherwise
 */
function removeWallets(req, res) {
  console.log('REMOVE_WALLETS: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Only the administrator can do this operation
      if (token.role === 'admin') {
        // Remove the wallets and their transactions from the database
        queries.deleteWallets(userId)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

// ----------------------------------------------------------------
// Wallets operations ---------------------------------------------

/**
 * [GET]
 * Get user's wallets
 *
 * @param {*} req headers(authorization)
 * @param {*} res json(CODE) and code 200 if ok, an error otherwise
 */
function getWallets(req, res) {
  console.log('GET_USERS_WALLETS: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  if (token !== undefined) {
    const userId = parseInt(token.salvationId);

    if (userId !== undefined) {
      // Get the wallets from the database
      queries.getUserWallets(userId)
        .then(result => sendResponse(res, result))
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

// ----------------------------------------------------------------
// Top up operations ----------------------------------------------

/**
 * [POST]
 * Top up an user's wallet
 *
 * @param {*} req params(userId), header(authorization), body(topup)
 * @param {*} res code 200, an error otherwise
 */
function topup(req, res) {
  console.log('WALLET_TOPUP: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const topup = req.body;
  const userId = parseInt(req.params.userId, 10);

  // Check information
  if (token !== undefined) {
    // Only the administrator or a topup account can do this operation
    if (token.role === 'admin' || token.role === 'topup') {
      if (userId !== undefined) {
        if (topup.amount !== undefined && topup.type !== undefined) { 
          // Top up the user's wallet in the database
          queries.topup(userId, topup.type, topup.amount)
            .then(result => sendResponse(res, result))
            .catch(err => sendSpecialError(err, res));
        } else {
          errorHandler.requestInformationMissing(res);
        }
      } else {
        errorHandler.userIdMissingInUrl(res);
      }
    } else {
      errorHandler.notAnAdminOrTopupAccount(res);
    }
  }
}

module.exports = {
  createWallets,
  removeWallets,
  getWallets,
  getUserWallets,
  topup,
};
