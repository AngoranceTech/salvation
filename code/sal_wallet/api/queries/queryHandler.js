/* global User */
/* eslint no-console: 0 */

var ObjectId = require('mongodb').ObjectID;

const errorHandler = require('../errors/errorHandler');
const errorMessage = require('../errors/errorMessage');

const dbWallet = Wallet.getDatastore().manager.collection('wallet');
const dbTransaction = Transaction.getDatastore().manager.collection('transaction');

const PAGE_SIZE = 10;

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * Create all user's wallets in the database
 * @param {*} wallets Wallets to create
 */
function createWallet(wallets) {
  return dbWallet.insert(wallets)
    .then((operation) => {
      // wallet has been created
      if (operation.insertedCount >= 1) {
        console.log('Wallets created');
        return true;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noWalletCreated);
    });
}

/**
 * Delete all user's wallets from the database
 * @param {*} id Id of the wallets owner to delete
 */
function deleteWallets(id) {

  // Delete the wallets
  return dbWallet.remove({ salvationId: id })
    .then(() => {
      // Wallets have been removed
      console.log('Wallets removed');
      return;
    })
    .catch(() => { throw new errorHandler.SpecialError(500, errorMessage.noWalletRemoved); });
}

// ----------------------------------------------------------------
// Wallets OPERATIONS ---------------------------------------------

/**
 * Retrieve all the user's wallets from the database
 * @param {*} userId Id of the user
 */
function getUserWallets(userId) {
  return dbWallet.find({ salvationId: userId }, { salvationId: 0 }).sort({ type: 1 }).toArray()
    .then((wallets) => {
      console.log('Wallets retrieved');
      return wallets;
    });
}

/**
 * Retrieve a wallet by its ID from the database
 * @param {*} walletId Id of the wallet
 */
function getWalletById(walletId) {
  console.log(`wallet id: ${walletId}, type: ${typeof walletId}`);

  return dbWallet.findOne({ _id: ObjectId(walletId) })
    .then((wallet) => {

      // Wallet has been found
      if (wallet != null) {
        console.log(`Wallet found: ${wallet} for id ${walletId}`);
        return wallet;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noWalletFound);
    });
}

// ----------------------------------------------------------------
// Transactions OPERATIONS ----------------------------------------

/**
 * Create a transaction in the database
 * @param {*} transaction Transaction to create 
 */
async function createTransaction(transaction) {
  let toId = transaction.to.salvationId;
  let fromId = transaction.from.salvationId;
  let txType = transaction.type;

  let toWallet = await dbWallet.findOne({ salvationId: toId, type: txType }, { _id: 1, amount: 1 });
  let fromWallet = await dbWallet.findOne({ salvationId: fromId, type: txType }, { _id: 1, amount: 1 });

  if (toWallet === undefined || fromWallet === undefined) {
    throw errorHandler.SpecialError(500, 'Problem occured fetching from/to in db');
  } else if (toWallet._id === undefined || fromWallet._id === undefined) {
    throw errorHandler.SpecialError(500, 'Problem occured. No wallet defined for one entity.');
  }

  let wallets = [
    toWallet._id,
    fromWallet._id,
  ];

  transaction.wallets = wallets;

  // First, we create the transaction in the database
  let operation = await dbTransaction.insertOne(transaction);

  if (operation === undefined) {
    throw errorHandler.SpecialError(500, 'Problem occured inserting transaction in db');
  }

  await updateAmount(toWallet, fromWallet, transaction.amount);

  return 'Success';
}

/**
 * Update the amount of wallets
 * @param {*} toWallet Destination wallet
 * @param {*} fromWallet Source wallet
 * @param {*} amount Amount of the transaction
 */
async function updateAmount(toWallet, fromWallet, amount) {
  let toAmount = toWallet.amount + amount;
  let fromAmount = fromWallet.amount - amount;

  let res1 = await dbWallet.update({ _id: toWallet._id }, { $set: { amount: toAmount } });
  let res2 = await dbWallet.update({ _id: fromWallet._id }, { $set: { amount: fromAmount } });

  if (res1 === undefined || res2 === undefined) {
    throw errorHandler.SpecialError(500, 'Problem occured while updating the database');
  }
}

/**
 * Check if a wallet has enough money to pay
 * @param {*} transaction Transaction to pay
 */
async function enoughMoney(transaction) {
  let fromId = transaction.from.salvationId;
  let amount = transaction.amount;
  let type = transaction.type;

  let owned = await dbWallet.findOne({ salvationId: fromId, type }, { amount: 1 });

  console.log(`owned: ${owned.amount}, amount: ${amount}`);

  return -1 * (owned.amount - amount);
}

/**
 * Retrieve transactions of a wallet
 * @param {*} walletId Id of the wallet
 * @param {*} pageNumber Page of the transactions to retrieve
 */
function getTransactionsByWalletId(walletId, pageNumber) {
  let skips = PAGE_SIZE * (pageNumber - 1);

  return dbTransaction.find({ wallets: ObjectId(walletId) }, { wallets: 0 }).sort({ _id: -1 }).skip(skips).limit(PAGE_SIZE).toArray();
}

/**
 * Topup a wallet
 * @param {*} userId User's ID
 * @param {*} type User's wallet type
 * @param {*} amount Amount of the topup
 */
async function topup(userId, type, amount) {

  // Get the user's wallet
  let wallet = await dbWallet.findOne({ salvationId: userId, type }, { _id: 1, amount: 1 });

  // Calculate the new amount
  let amountUpdated = wallet.amount + amount;

  // Top up the wallet
  return dbWallet.update({ _id: wallet._id }, { $set: { amount: amountUpdated } })
    .then((result) => {

      // Wallet has been found
      if (result !== undefined) {
        console.log(`Wallet has been top up`);
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noTopUpDone);
    });
}

module.exports = {
  getUserWallets,
  getWalletById,
  createWallet,
  deleteWallets,
  createTransaction,
  enoughMoney,
  getTransactionsByWalletId,
  topup,
};
