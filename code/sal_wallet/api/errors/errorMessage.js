// ----------------------------------------------------------------
// QUERY HANDLER --------------------------------------------------
const nonExistentWallet = 'This wallet doesn\'t exist';
const noWalletCreated = 'No wallet has been created';
const noWalletUpdated = 'No wallet has been updated';
const noWalletFound = 'No wallet has been found';
const noWalletRemoved = 'No wallet has been removed';
const noTopUpDone = 'No top up has been done';

// ----------------------------------------------------------------
// 400 - BAD REQUEST ----------------------------------------------

const userIdMissingInUrl = 'No user ID provided in the url';
const requestInformationMissing = 'Request format not respected';

// ----------------------------------------------------------------
// 401 - UNAUTHORIZED ---------------------------------------------

const tokenIsIncomplete = 'Token not complete';
const tokenIsMissing = 'No token provided';
const tokenIsInvalid = 'The token provided isn\'t valid';

// ----------------------------------------------------------------
// 403 - FORBIDDEN ------------------------------------------------
const notAnAdmin = 'Only an administrator can do this';
const notAnAdminOrTopupAccount = 'Only an administrator or a topup account can do this';

function notEnoughMoney(amount) {
  return `Not enough money for transaction. ${amount} needed!`;
}

module.exports = {
  nonExistentWallet,
  noWalletCreated,
  noWalletUpdated,
  noWalletFound,
  noWalletRemoved,
  userIdMissingInUrl,
  requestInformationMissing,
  tokenIsIncomplete,
  tokenIsMissing,
  tokenIsInvalid,
  notAnAdmin,
  notAnAdminOrTopupAccount,
  notEnoughMoney,
  noTopUpDone
};
