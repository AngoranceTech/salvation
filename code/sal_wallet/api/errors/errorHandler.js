const errorMessage = require('./errorMessage');

// ----------------------------------------------------------------
// SPECIAL ERROR --------------------------------------------------
class SpecialError extends Error {
  constructor(code, err) {
    super();
    this.code = code;
    this.err = err;
  }
}

// ----------------------------------------------------------------
// SEND ERROR -----------------------------------------------------
function sendError(err, res, code) {

  let message = '';

  if (err.name !== undefined && err.name === 'MongoError') {
    code = 500; // Server error (mongo)
    message = err.message;
  } else {
    message = err;
  }

  console.log(code + ' - ' + message);

  // Send the error to the client
  res.status(code);
  res.send(message);
}

// ----------------------------------------------------------------
// 400 - BAD REQUEST ----------------------------------------------

function userIdMissingInUrl(res) {
  sendError(errorMessage.userIdMissingInUrl, res, 400);
}

function requestInformationMissing(res) {
  sendError(errorMessage.requestInformationMissing, res, 400);
}

// ----------------------------------------------------------------
// 401 - UNAUTHORIZED ---------------------------------------------

function tokenIsIncomplete(res) {
  sendError(errorMessage.tokenIsIncomplete, res, 401);
}

function tokenIsMissing(res) {
  sendError(errorMessage.tokenIsMissing, res, 401);
}

function tokenIsInvalid(res) {
  sendError(errorMessage.tokenIsInvalid, res, 401);
}

// ----------------------------------------------------------------
// 403 - FORBIDDEN ------------------------------------------------

function notAnAdmin(res) {
  sendError(errorMessage.notAnAdmin, res, 403);
}

function notAnAdminOrTopupAccount(res) {
  sendError(errorMessage.notAnAdminOrTopupAccount, res, 403);
}

function notEnoughMoney(res, amountNeeded) {
  sendError(errorMessage.notEnoughMoney(amountNeeded), res, 403);
}

module.exports = {
  SpecialError,
  sendError,
  userIdMissingInUrl,
  requestInformationMissing,
  tokenIsIncomplete,
  tokenIsMissing,
  tokenIsInvalid,
  notAnAdmin,
  notAnAdminOrTopupAccount,
  notEnoughMoney,
};