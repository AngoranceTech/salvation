# Salvation - API - Wallet service

Authors : Héléna Reymond, Daniel Gonzalez Lopez, Bryan Curchod, François Burgener

This is a document which describes the **Wallet** service.

---

## Description

This service is used by the administrator and clients to manage wallets and transactions on Salvation.

```javascript
// Model of the user's wallet
{
  salvationId: {
    type: 'number',
    required: true,
  },
  amount: {
    type: 'number',
    required: true,
  },
  type: {
    type: 'string',
    required: true,
  }
}

// Model of the wallet transaction
{
  amount: {
    type: 'number',
    required: true,
  },
  from: {
    type: 'json',
    required: true,
  },
  to: {
    type: 'json',
    required: true,
  },
  type: {
    type: 'string',
    required: true,
  },
  description: {
    type: 'string',
    required: true,
  },
  date: {
    type: 'string',
    required: true,
  },
  wallets: {
    type: 'json',
    required: true,
  }
}
```

---

## Technologies used

### Backend
- <u>Framework</u>: Sails
- <u>Dependencies</u>: jsonwebtoken, eslint

### MongoDB Database
- <u>Database</u>: walletDB
- <u>Collections</u>: wallet, transaction

### API tests
- Mocha
- Chai
- Axios

---

## API

Here are the APIs specifications of our service.
The complete specification is available in `/specs` folder.

### Url of the service
```
https://salvation.angorance.tech/api/wallet
```

### Endpoints

```javascript
// Admin
POST   =>      '/wallets/{userId}'                  // Create a wallet
GET    =>      '/wallets/{userId}'                  // List all user's wallets
DELETE =>      '/wallets/{userId}'                  // Delete a wallet

// Wallets
GET    =>      '/wallets/'                          // Retrieve the connected user's wallets

// Transactions
POST   =>      '/transactions'                      // Create a transaction
GET    =>      '/wallets/{walletId}/transactions'   // Retrieve all the transactions of a wallet

// Top up
POST   =>      'topup/{userId}'                     // Top up the wallet of a user
```

---

## Functional tests

The goal of these `32 tests` is to verify all the endpoints of our service.

### CRUD operations on wallets

<img src="wallet_crud_tests.PNG">

### CRUD operations on transactions

<img src="transaction_crud_tests1.PNG">
<img src="transaction_crud_tests2.PNG">