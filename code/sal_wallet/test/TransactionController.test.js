const { expect } = require('chai');
const sails = require('sails');
const axios = require('axios');
const errorMessage = require('../api/errors/errorMessage');

const client = axios.create({
  baseURL: 'http://localhost:1337',
  timeout: 2000,
});

const authenticationService = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/auth',
  timeout: 2000,
});

describe('WALLET SERVICE - TRANSACTION ----------------------', () => {

  /* ========================================================================
  /*  LOGIN (role admin)
  /*====================================================================== */

  let token = '';
  let adminId = -1;

  it('should log in the administrator', () => {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'admin@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        token = response.data.jwt;
        salvationId = response.data.salvationId;
        adminId = salvationId;

        const expected = { jwt: token, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch(error => console.log(error.data));
  });

  /* ========================================================================
  /*  LOGIN (role student)
  /*====================================================================== */

  let tokenUser = '';
  let userId = -1;

  it('should log in the student test', () => {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'test@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        tokenUser = response.data.jwt;
        salvationId = response.data.salvationId;
        userId = salvationId;

        const expected = { jwt: tokenUser, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch((error) => {
        console.log(error.data);
        Promise.reject();
      });
  });

  /* ========================================================================
  /*  ADMIN'S WALLETS ID RETRIEVAL (role admin)
  /*====================================================================== */
  let mainAdminWalletId = -1;
  let printAdminWalletId = -1;

  it('should retrieve ID of the admin\'s wallets (role admin)', () => {

    console.log('WALLETS ID RETRIEVAL:');

    const header = { headers: { Authorization: token } };

    return client
        .get(`/wallets`, header)
        .then((response) => {

          const wallets = response.data;

          // Get id of each wallet
          for (let i in wallets) {

            if (wallets[i].type === 'print') {
              printAdminWalletId = wallets[i]._id;
              console.log(`Print admin wallet id : ${printAdminWalletId}`);

            } else if (wallets[i].type === 'main') {
              mainAdminWalletId = wallets[i]._id;
              console.log(`Main admin wallet id : ${mainAdminWalletId}`);
            }
          }

          expect(printAdminWalletId).to.not.eql(-1);
          expect(mainAdminWalletId).to.not.eql(-1);
        });
  });

  /* ========================================================================
  /*  STUDENT'S WALLETS ID RETRIEVAL (role admin)
  /*====================================================================== */
  let mainWalletId = -1;
  let printWalletId = -1;

  it('should retrieve ID of the student test\'s wallets (role admin)', () => {

    console.log('WALLETS ID RETRIEVAL:');

    const header = { headers: { Authorization: token } };

    return client
        .get(`/wallets/${userId}`, header)
        .then((response) => {

          const wallets = response.data;

          // Get id of each wallet
          for (let i in wallets) {

            if (wallets[i].type === 'print') {
              printWalletId = wallets[i]._id;
              console.log(`Print wallet id : ${printWalletId}`);

            } else if (wallets[i].type === 'main') {
              mainWalletId = wallets[i]._id;
              console.log(`Main wallet id : ${mainWalletId}`);
            }
          }

          expect(printWalletId).to.not.eql(-1);
          expect(mainWalletId).to.not.eql(-1);
        });
  });

  /* ========================================================================
  /*  TRANSACTIONS OF STUDENT'S MAIN WALLET RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get(`/wallets/${mainWalletId}/transactions?pageNumber=1`, {})
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve an empty list of transaction for user test\'s main wallet (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/wallets/${mainWalletId}/transactions?pageNumber=1`, header)
      .then((response) => {
        const expected = [];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  TRANSACTION CREATION ON STUDENT'S AND ADMIN'S MAIN WALLET (role admin)
  /*====================================================================== */

  it('should say that the token is missing', () => {
    return client
      .post(`/transactions`, {})
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should say that a field is missing (role admin)', () => {
    const data = {
      'amount': 1200,
      'from': {
        'name': 'User test'
      },
      'to': {
        'salvationId': adminId
      },
      'type': 'main',
      'description': 'Course payment'
    };
    const header = { headers: { Authorization: token } };

    return client
      .post(`/transactions`, data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.requestInformationMissing);
      });
  });

  it('should create a transaction on student\´s and admin\'s main wallet (role admin)', () => {
    const data = {
      'amount': 1200,
      'from': {
        'salvationId': userId,
        'name': 'User test'
      },
      'to': {
        'salvationId': adminId
      },
      'type': 'main',
      'description': 'Course payment'
    };
    const header = { headers: { Authorization: token } };

    return client
      .post(`/transactions`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  it('should create a transaction with the email on student\´s and admin\'s main wallet (role admin)', () => {
    const data = {
      'amount': 400,
      'from': {
        'salvationId': userId,
        'name': 'User test'
      },
      'to': {
        'email': 'admin@salvation.ch'
      },
      'type': 'main',
      'description': 'Course payment by email'
    };
    const header = { headers: { Authorization: token } };

    return client
      .post(`/transactions`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  it('should create a transaction with the email on student\´s and admin\'s main wallet (role admin)', () => {
    const data = {
      'amount': 200,
      'from': {
        'salvationId': userId,
        'name': 'User test'
      },
      'to': {
        'email': 'admin@salvation.ch'
      },
      'type': 'main',
      'description': 'Course payment by email'
    };
    const header = { headers: { Authorization: token } };

    return client
      .post(`/transactions`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  TRANSACTIONS OF ADMIN'S MAIN WALLET RETRIEVAL (role admin)
  /*====================================================================== */

  it('should retrieve the list of transactions for admin\'s main wallet (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/wallets/${mainAdminWalletId}/transactions?pageNumber=1`, header)
      .then((response) => {
        const expected = [{
          '_id': response.data[0]._id,
          'amount': 200,
          'from': {
            'salvationId': userId,
            'name': 'User test'
          },
          'to': {
            'email': 'admin@salvation.ch',
            'salvationId': adminId,
            'name': response.data[0].to.name
          },
          'type': 'main',
          'description': 'Course payment by email',
          'date': response.data[0].date
        },
        {
          '_id': response.data[1]._id,
          'amount': 400,
          'from': {
            'salvationId': userId,
            'name': 'User test'
          },
          'to': {
            'email': 'admin@salvation.ch',
            'salvationId': adminId,
            'name': response.data[1].to.name
          },
          'type': 'main',
          'description': 'Course payment by email',
          'date': response.data[1].date
        },
        {
          '_id': response.data[2]._id,
          'amount': 1200,
          'from': {
            'salvationId': userId,
            'name': 'User test'
          },
          'to': {
            'salvationId': adminId,
            'name': response.data[2].to.name
          },
          'type': 'main',
          'description': 'Course payment',
          'date': response.data[2].date
        }];
        
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  TRANSACTIONS OF STUDENT'S MAIN WALLET RETRIEVAL (role student)
  /*====================================================================== */

  it('should retrieve the list of transactions for user test\'s main wallet (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .get(`/wallets/${mainWalletId}/transactions?pageNumber=1`, header)
      .then((response) => {
        const expected = [{
          '_id': response.data[0]._id,
          'amount': 200,
          'from': {
            'salvationId': userId,
            'name': 'User test'
          },
          'to': {
            'email': 'admin@salvation.ch',
            'salvationId': adminId,
            'name': response.data[0].to.name
          },
          'type': 'main',
          'description': 'Course payment by email',
          'date': response.data[0].date
        },
        {
          '_id': response.data[1]._id,
          'amount': 400,
          'from': {
            'salvationId': userId,
            'name': 'User test'
          },
          'to': {
            'email': 'admin@salvation.ch',
            'salvationId': adminId,
            'name': response.data[1].to.name
          },
          'type': 'main',
          'description': 'Course payment by email',
          'date': response.data[1].date
        },
        {
          '_id': response.data[2]._id,
          'amount': 1200,
          'from': {
            'salvationId': userId,
            'name': 'User test'
          },
          'to': {
            'salvationId': adminId,
            'name': response.data[2].to.name
          },
          'type': 'main',
          'description': 'Course payment',
          'date': response.data[2].date
        }];
        
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* ADMIN'S WALLETS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should retrieve the list of wallets for admin (role admin)', function () {
    const header = { headers: { Authorization: token } };
    return client
      .get('/wallets', header)
      .then((response) => {
        const expected = [
          
          {"_id": response.data[0]._id, "amount": 101800, "type": "main"},
          {"_id": response.data[1]._id, "amount": 0, "type": "print"}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* STUDENT'S WALLETS RETRIEVAL (role student)
  /*====================================================================== */

  it('should retrieve the list of wallets for the user test (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };
    return client
      .get('/wallets', header)
      .then((response) => {
        const expected = [
          
          {"_id": response.data[0]._id, "amount": 200, "type": "main"},
          {"_id": response.data[1]._id, "amount": 50, "type": "print"}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /**************************************************************************************** */
  /* END OF THE WALLET TESTS 


  /* ========================================================================
  /*  WALLETS REMOVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .delete(`/wallets/${userId}`, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  WALLETS REMOVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .delete(`/wallets/${userId}`)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should remove test\'s wallets (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .delete(`/wallets/${userId}`, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  WALLETS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should retrieve an empty list of test\'s wallets (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/wallets/${userId}`, header)
      .then((response) => {
        const expected = [];
        expect(response.data).to.eql(expected);
      });
  });
});
