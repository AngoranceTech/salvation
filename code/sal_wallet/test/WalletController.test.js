const { expect } = require('chai');
const sails = require('sails');
const axios = require('axios');
const errorMessage = require('../api/errors/errorMessage');

const client = axios.create({
  baseURL: 'http://localhost:1337',
  timeout: 2000,
});

const authenticationService = axios.create({
  baseURL: 'https://salvation.angorance.tech/api/auth',
  timeout: 2000,
});

describe('WALLET SERVICE - WALLET ----------------------', function () {

  /* ========================================================================
  /*  LOGIN (role admin)
  /*====================================================================== */

  let token = "";

  it('should log in the administrator', function () {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'admin@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        token = response.data.jwt;
        salvationId = response.data.salvationId;

        const expected = { jwt: token, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch((error) => {
        console.log(error.data);
        Promise.reject();
      });
  });

  /* ========================================================================
  /*  LOGIN (role student)
  /*====================================================================== */

  let tokenUser = "";
  let userId = -1;

  it('should log in the student test', function () {

    console.log('LOGIN:');

    return authenticationService
      .post('/authentication', { email: 'test@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        tokenUser = response.data.jwt;
        salvationId = response.data.salvationId;
        userId = salvationId;

        const expected = { jwt: tokenUser, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      })
      .catch((error) => {
        console.log(error.data);
        Promise.reject();
      });
  });

  /* ========================================================================
  /* ADMIN'S WALLETS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get('/wallets')
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve the list of wallets for admin (role admin)', function () {
    const header = { headers: { Authorization: token } };
    return client
      .get('/wallets', header)
      .then((response) => {
        const expected = [
          
          {"_id": response.data[0]._id, "amount": 100000, "type": "main"},
          {"_id": response.data[1]._id, "amount": 0, "type": "print"}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* STUDENT'S WALLETS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get(`/wallets/${userId}`, {})
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve an empty list of wallets for user test (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/wallets/${userId}`, header)
      .then((response) => {
        const expected = [];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* WALLETS CREATION (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .post(`/wallets/${userId}`, {})
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should say that a field is missing (role admin)', function () {
    const data = [
      {"amount": 0, "type": "main"},
      {"amount": 0}
    ];
    const header = { headers: { Authorization: token } };

    return client
      .post(`/wallets/${userId}`, data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.requestInformationMissing);
      });
  });

  it('should create wallets for user test (role admin)', function () {
    const data = [
      
      {"amount": 2000, "type": "main"},
      {"amount": 0, "type": "print"}
    ];
    const header = { headers: { Authorization: token } };

    return client
      .post(`/wallets/${userId}`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /* STUDENT'S WALLETS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should retrieve the list of wallets for user test (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/wallets/${userId}`, header)
      .then((response) => {
        const expected = [          
          {"_id": response.data[0]._id, "amount": 2000, "type": "main"},
          {"_id": response.data[1]._id, "amount": 0, "type": "print"}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* WALLETS CREATION (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const data = [
      {"amount": 0, "type": "main"},
      {"amount": 0, "type": "print"}
    ];
    const header = { headers: { Authorization: tokenUser } };

    return client
      .post(`/wallets/${userId + 1}`, {}, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /* WALLETS RETRIEVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .get(`/wallets/${userId}`, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  it('should retrieve the list of wallets for user test (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .get(`/wallets`, header)
      .then((response) => {
        const expected = [          
          {"_id": response.data[0]._id, "amount": 2000, "type": "main"},
          {"_id": response.data[1]._id, "amount": 0, "type": "print"}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* WALLET TOP UP (role admin)
  /*====================================================================== */

  it('should top up the print account of the student (role admin)', function () {
    const data = {"amount": 50, "type": "print"};
    const header = { headers: { Authorization: token } };

    return client
      .post(`/topup/${userId}`, data, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  it('should retrieve the list of wallets for user test (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/wallets/${userId}`, header)
      .then((response) => {
        const expected = [          
          {"_id": response.data[0]._id, "amount": 2000, "type": "main"},
          {"_id": response.data[1]._id, "amount": 50, "type": "print"}
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /* WALLET TOP UP (role student)
  /*====================================================================== */

  it('should say that only an administrator or a topup account can do this (role student)', function () {
    const data = {"amount": 0, "type": "print"};
    const header = { headers: { Authorization: tokenUser } };

    return client
      .post(`/topup/${userId}`, data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdminOrTopupAccount);
      });
  });
});