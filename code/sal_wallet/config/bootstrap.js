/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function (done) {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return done();
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  // Don't forget to trigger `done()` when this bootstrap function's logic is finished.
  // (otherwise your server will never lift, since it's waiting on the bootstrap)
  sails.on('lifted', () => {

    let dbWallet = Wallet.getDatastore().manager.collection('wallet');

    //Create a unique index for email
    dbWallet.createIndex({'salvationId': 1, 'type': 1}, { unique: true });

    // Create a main admin for cafeteria
    dbWallet.insertOne({ amount: 100000, type: 'main', salvationId: 0 });
    dbWallet.insertOne({ amount: 0, type: 'print', salvationId: 0 });

    // Create a main wallet for cafeteria
    dbWallet.insertOne({ amount: 0, type: 'main', salvationId: 2 });

    // Create a main wallet for parking
    dbWallet.insertOne({ amount: 0, type: 'main', salvationId: 3 });

    // Create a print wallet for printer
    dbWallet.insertOne({ amount: 0, type: 'print', salvationId: 4 });
  });

  return done();
};
