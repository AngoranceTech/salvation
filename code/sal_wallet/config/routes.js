/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /**
   * Admin
   *
   * Allow admins to add and delete wallets for a user
   */
  'GET      /wallets/:userId': 'WalletController.getUserWallets',
  'POST     /wallets/:userId': 'WalletController.createWallets',
  'DELETE   /wallets/:userId': 'WalletController.removeWallets',

  /**
   * Wallets
   *
   * Allows users to get their wallets.
   */
  'GET      /wallets/': 'WalletController.getWallets',

  /**
   * Transactions
   *
   * Allows users or services to create and consult transactions
   */
  'POST     /transactions': 'TransactionController.createTransaction',
  'GET      /wallets/:walletId/transactions': 'TransactionController.getTransactionsByWallet',

  /**
   * Top up
   *
   * Allows admin and topup accounts to top up wallets
   */
  'POST     /topup/:userId': 'WalletController.topup',
};
