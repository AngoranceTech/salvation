# Salvation - Checkout mobile application
Authors : Burgener François, Curchod Bryan, Gonzalez Lopez Daniel, Reymond Héléna

This document describes the Checkout mobile application.

## Description
The checkout application aims to simulate a transaction with a NFC card. The main usage of such a practice will be in the cafeteria, when a student want to pay his/her morning croissant, or whatever he/she can find pleasing.

## User guide
At launch you'll land on the checkout application. The first step is to fill the cart with something that the student want. To choose those items, simply click on the `add item` button available at the top of the screen. You'll then find a liste of the possible buyable item. 

![cart filling view](readme-src/cart-list.jpg)

Simply tap on the item that you want to add in the cartlist. In th case that you make a mistake and remove an unit of an item, the `minus button` is available at the right. Once you're done you can confirm the cart with the button at the bottom of the screen (or the back button of your smartphone). You'll get back to the previous view, but this time you'll have a cart list available with only the item you selected.

![cart filling view](readme-src/checkout-cart.jpg)

Now the next step is to pay for your cart. You can start the NFC tag reading, by clicking on the `checkout` button. Then the application will wait for a NFC tag to read it and start a transaction. The NFC tag has to adopt the following structure to be understood : 
```json
{ 
    "userid" : 42, 
    "fName" : "Ronflex", 
    "lName" : "Ample"
}
```

While the application is waiting for a card, it disable the `Add item` button, you can cancel the operation by clicking again on the checkout button (now named `cancelOperation`). When a card with the right format is read the application is "locked", it sends a transction to pay the cart. You have to wait for the operation to finish before doing anything else.