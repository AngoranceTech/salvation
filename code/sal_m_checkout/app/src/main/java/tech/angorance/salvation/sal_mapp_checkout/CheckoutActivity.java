package tech.angorance.salvation.sal_mapp_checkout;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tech.angorance.salvation.sal_mapp_checkout.Item.Buyable;
import tech.angorance.salvation.sal_mapp_checkout.Item.ItemDisplayer;

/**
 * Main activity, summarize the user's cart and allows to make a transaction
 * The transactions are made by scanning a NFC card containing the right data
 * The code for the nfc part were taken here : https://code.tutsplus.com/tutorials/reading-nfc-tags-with-android--mobile-17278
 */
public class CheckoutActivity extends AppCompatActivity implements RequestSender {
    private static final int REQ_CODE = 1;
    private NfcAdapter nfcAdapter = null;
    final NumberFormat formatter = new DecimalFormat("#0.00");
    List<Buyable> cart;

    Button btnSelectItem, btnCheckout;
    LinearLayout cartContentList;
    TextView txtTotal;

    boolean reading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        cartContentList = findViewById(R.id.listItem);
        txtTotal = findViewById(R.id.txtTotal);
        btnSelectItem = findViewById(R.id.btnAddItem);
        btnCheckout = findViewById(R.id.btnCheckout);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;

        }

        if (!nfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC is disabled.", Toast.LENGTH_SHORT).show();
        }

        btnSelectItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO launch next activity
                Intent i = new Intent(CheckoutActivity.this, ItemSelectionActivity.class);
                i.putExtra("requestCode", REQ_CODE);
                startActivityForResult(i, REQ_CODE);
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButtonCheckout();
            }
        });
    }

    private void toggleButtonCheckout() {
        reading = !reading;

        if(cart.isEmpty() && reading){
            reading = false;
            Toast.makeText(this, "Please select an item to buy", Toast.LENGTH_SHORT).show();
        }

        btnCheckout.setText(reading ? "Cancel operation" : "Checkout");
        btnSelectItem.setEnabled(!reading);

        if(reading){
            setupForegroundDispatch(CheckoutActivity.this, nfcAdapter);
            Toast.makeText(CheckoutActivity.this, "NFC Reading activated", Toast.LENGTH_SHORT).show();
        } else {
            stopForegroundDispatch(CheckoutActivity.this, nfcAdapter);
            Toast.makeText(CheckoutActivity.this, "NFC Reading stopped", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE){
            cart = new ArrayList<>();
            String sCart = data.getExtras().getString("CART");
            try {
                JSONArray jsonCart = new JSONArray(sCart);
                for(int i = 0; i < jsonCart.length(); ++i){
                    JSONObject item = jsonCart.getJSONObject(i);
                    cart.add(new Buyable(item.getString("name"), item.getDouble("price"), item.getInt("quantity")));
                }

                updateCartDisplay();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            // no good, how ???
        }
    }

    private void updateCartDisplay() {
        cartContentList.removeAllViews();
        for(Buyable b : cart) {
            ItemDisplayer d = new ItemDisplayer(this, b);
            d.setRemoveAction(v -> {cart.remove(b); updateCartDisplay();});

            cartContentList.addView(d);
        }
        txtTotal.setText("Total : " + formatter.format(getTotalCart()));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if ("text/plain".equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask().execute(tag);

            } else {
                Log.d("NFC-READING", "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }

    /**
     * Method called to handle the NFC tag data
     * Check the mail, the password and the tag used
     * @param data NFC tag data
     */
    public void handleTag(String data) {
        // TODO SEND REQUEST
        JSONObject jsonData = null;
        try {
            jsonData = new JSONObject(data);
            CardDataDTO cardData = new CardDataDTO(jsonData.getInt("userid"), jsonData.getString("fName"), jsonData.getString("lName"));

            RequestWorker reqSender = new RequestWorker();


            stopForegroundDispatch(this, nfcAdapter);
            btnCheckout.setEnabled(false);
            btnCheckout.setText("waiting for response...");

            reqSender.sendRequest(cardData, getTotalCart(), getCartString(), this);

            Toast.makeText(this, "Please wait...", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonData == null){
            System.out.println("pas bon poto");
            Toast.makeText(this, "Can't use card data in JSON", Toast.LENGTH_SHORT).show();
            finish();
        }


    }

    private Double getTotalCart(){
        return cart.size() > 0 ? cart.stream().map(item -> {return item.getPrice() * item.getQuantity();}).reduce((Double a, Double b) -> a+b).get() : 0.0;
    }

    private String getCartString(){
        return cart.stream()
                .map(buyable ->{return buyable.getQuantity() + "x " + buyable.getName() + ": " + buyable.getQuantity() * buyable.getPrice();})
                .reduce((a, b) -> a + '\n'+ b)
                .get();
    }

    /**
     * @param activity The corresponding {@link Activity} requesting the foreground dispatch.
     * @param adapter The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    /**
     * @param activity The corresponding BaseActivity requesting to stop the foreground dispatch.
     * @param adapter The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    @Override
    public void handleResponse(String data, int code) {
        if(code == 200){
            Toast.makeText(this, "Transaction successful", Toast.LENGTH_SHORT).show();

            btnSelectItem.setEnabled(true);
            btnCheckout.setEnabled(true);

            cart.clear();
            updateCartDisplay();
        } else {
            Toast.makeText(this, "An error occured...", Toast.LENGTH_SHORT).show();
        }
        toggleButtonCheckout();
        System.out.println(data);
    }


    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        // object that will handle the tag data
        NdefReaderTask(){
            super();

        }

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
            /*
             * See NFC forum specification for "Text Record Type Definition" at 3.2.1
             *
             * http://www.nfc-forum.org/specs/
             *
             * bit_7 defines encoding
             * bit_6 reserved for future use, must be 0
             * bit_5..0 length of IANA language code
             */

            byte[] payload = record.getPayload();

            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                CheckoutActivity.this.handleTag(result);
            }
        }
    }

}
