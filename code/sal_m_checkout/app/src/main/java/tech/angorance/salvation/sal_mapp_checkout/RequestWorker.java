package tech.angorance.salvation.sal_mapp_checkout;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static java.lang.Thread.sleep;

public class RequestWorker extends AsyncTask<String, Void, String> {
    final String url = "https://salvation.angorance.tech/api/wallet/transactions";
    private OkHttpClient client = new OkHttpClient(); // gère les communications avec le serveur
    private RequestSender reqReturn;
    private String token = "";
    private int codeResult;

    void sendRequest(CardDataDTO fromData, double amount, String itemList, RequestSender r){
        JSONObject root = new JSONObject();
        try {
            JSONObject to = new JSONObject().put("email", "cafeteria@salvation.ch");
            JSONObject from = new JSONObject().put("salvationId", fromData.getUserId()).put("name", fromData.getFirstName() + " " + fromData.getLastName());
            root.put("amount", amount)
                .put("from", from)
                .put("to", to)
                .put("description", itemList)
                .put("type", "main");

            reqReturn = r;
            execute(root.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * send the transaction request
     * @param strings parameters
     * @return
     */
    @Override
    protected String doInBackground(String... strings) {
        // corps de la requête

        if(token == ""){
            token = getToken();
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), strings[0]);

        // construction de la requête
        Request.Builder reqBuild = new Request.Builder();
        reqBuild.url(url).post(body).addHeader("Authorization", token);

        Request req = reqBuild.build();
        String result = "";
        try {
            // envoi de la requête et réception de la réponse
            Response response = client.newCall(req).execute();
            // vérification du succès de la requête
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            result = "An error happened";
        }

        return result;
    }

    private String getToken() {
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), "{\"email\" : \"cafeteria@salvation.ch\", \"password\" : \"P@ssw0rd\"}");

        // construction de la requête
        Request.Builder reqBuild = new Request.Builder();
        reqBuild.url("https://salvation.angorance.tech/api/auth/authentication").post(body);

        Request req = reqBuild.build();
        try {
            // envoi de la requête et réception de la réponse
            Response response = client.newCall(req).execute();
            // vérification du succès de la requête
            JSONObject result = new JSONObject(response.body().string());
            codeResult = response.code();
            return result.getString("jwt");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * check for internet connection
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try{
            while(!isConnected()){
                System.out.println("Waiting for internet connection");
                sleep(5000); // aucune connexion n'est disponible on retente 5 secondes plus tard
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param s
     */
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println(s);
        reqReturn.handleResponse(s, codeResult);
    }

    /**
     * Vérifie la connexion à internet
     * @return true si l'appareil est connecté
     */
    private boolean isConnected(){
        try {
            int timeoutMs = 1500;
            Socket sock = new Socket();

            // on tente une connexion au DNS de Google
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) { return false; }
    }
}
