package tech.angorance.salvation.sal_mapp_checkout.Item;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Buyable implements Serializable {
    private String name;
    private int quantity;
    private double price;

    NumberFormat formatter = new DecimalFormat("#0.00");

    public Buyable(String n, double p){
        name = n;
        price = p;
        quantity=0;
    }

    public Buyable(String n, double p, int qty){
        name = n;
        price = p;
        quantity=qty;
    }

    public int getQuantity() {
        return quantity;
    }

    public void addOne(){quantity++;}
    public void removeOne(){
        if(quantity > 0){
            quantity--;
        }
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " - " + formatter.format(price);
    }

    public JSONObject serialize(){
        JSONObject serialized = new JSONObject();
        try {
            serialized.put("name", this.name);
            serialized.put("price", this.price);
            serialized.put("quantity", this.quantity);

            return serialized;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
