package tech.angorance.salvation.sal_mapp_checkout;

public class CardDataDTO {
    private  int userId;
    private String firstName;
    private String lastName;

    CardDataDTO(int id, String fName, String lName){
        userId = id;
        firstName = fName;
        lastName = lName;
    }

    public int getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
