package tech.angorance.salvation.sal_mapp_checkout.Item;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ItemDisplayer extends LinearLayout {
    private Buyable buyable;
    private TextView name;
    private TextView quantity;
    private Button remove;

    public ItemDisplayer(Context context, Buyable i) {
        super(context);

        this.setOrientation(HORIZONTAL);
        buyable = i;

        name = new TextView(context);
        quantity= new TextView(context);
        remove = new Button(context);

        name.setText(buyable.toString());
        quantity.setText(Integer.toString(buyable.getQuantity()));
        remove.setText("-");

        updateDisplay();
        quantity.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));

        this.addView(name);
        this.addView(quantity);
        this.addView(remove);
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void setRemoveAction(OnClickListener l){
        remove.setOnClickListener(l);
    }

    public Buyable getBuyable() {
        return buyable;
    }

    public void updateDisplay(){
        quantity.setText("x" + buyable.getQuantity());
    }
}
