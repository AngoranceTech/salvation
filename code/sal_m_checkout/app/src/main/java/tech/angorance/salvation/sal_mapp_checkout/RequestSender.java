package tech.angorance.salvation.sal_mapp_checkout;

interface RequestSender {
    void handleResponse(String data, int code);
}
