package tech.angorance.salvation.sal_mapp_checkout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import org.json.JSONArray;

import java.util.ArrayList;

import tech.angorance.salvation.sal_mapp_checkout.Item.Buyable;
import tech.angorance.salvation.sal_mapp_checkout.Item.ItemDisplayer;

public class ItemSelectionActivity extends AppCompatActivity {
    int reqCode;
    Buyable[] availableBuyables = {
            new Buyable("Croissant", 1.2),
            new Buyable("Soda", 2.5),
            new Buyable("Pain au chocolat", 2.2),
            new Buyable("Menu", 9.8),
            new Buyable("Energy Drink", 3.5),
            new Buyable("Coffee", 1.5),
            new Buyable("Mineral Water", 2.0),
            new Buyable("Panini", 5.0),

    };

    LinearLayout listLayout;
    Button validateCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_selection);

        validateCart = findViewById(R.id.btnValidate);
        listLayout = findViewById(R.id.listLayout);
        reqCode = getIntent().getExtras().getInt("requestCode");

        for(Buyable i : availableBuyables){
            ItemDisplayer displayer = new ItemDisplayer(this, i);
            displayer.setOnClickListener(v -> {displayer.getBuyable().addOne(); displayer.updateDisplay();});
            displayer.setRemoveAction(v -> {displayer.getBuyable().removeOne(); displayer.updateDisplay();});

            listLayout.addView(displayer);
        }

        validateCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent cartList = new Intent();

        cartList.putExtra("CART", getCart());
        setResult(reqCode, cartList);
        super.onBackPressed();
        finish();
    }

    private String getCart() {
        JSONArray cart = new JSONArray();

        for(Buyable i : availableBuyables){
            if (i.getQuantity() != 0){
                cart.put(i.serialize());
            }
        }

        return cart.toString();
    }
}
