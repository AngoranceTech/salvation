# Salvation - API - Authentication service

Authors : Héléna Reymond, Daniel Gonzalez Lopez, Bryan Curchod, François Burgener

This is a document which describes the **Authentication** service.

---

## Description

This service is used by the administrator and clients to manage connection accounts on Salvation.

```javascript
// Model of the user's account
{
  salvationId: {
    type: 'number',
    unique: true,
    required: true,
  },
  email: {
    type: 'string',
    unique: true,
    required: true,
  },
  password: {
    type: 'string',
    required: true,
  },
  role: {
    type: 'string',
    required: true,
  }
}
```

---

## Technologies used

### Backend
- <u>Framework</u>: Sails
- <u>Dependencies</u>: bcrypt, jsonwebtoken, eslint

### MongoDB Database
- <u>Database</u>: connectionDB
- <u>Collections</u>: user

### API tests
- Mocha
- Chai
- Axios

---

## API

Here are the APIs specifications of our service.
The complete specification is available in `/specs` folder.

### Url of the service
```
https://salvation.angorance.tech/api/auth
```

### Endpoints

```javascript
// Admin
POST   =>      '/registration'          // Create a user
GET    =>      '/accounts'              // List all users
PATCH  =>      '/accounts/{userId}'     // Modify a user
DELETE =>      '/accounts/{userId}'     // Delete a user

// Login
POST   =>      '/authentication'        // Login a user

// Accounts
GET    =>      '/accounts/{userId}'     // Retrieve a user
PATCH  =>      '/accounts/{userId}'     // Modify a user
```

---

## Functional tests

The goal of these `25 tests` is to verify all the endpoints of our service.

### Login operation

<img src="login_tests.PNG">

### CRUD operations with admin role

<img src="crud_tests.PNG">

### CRUD operations with student role

<img src="student_tests.PNG">