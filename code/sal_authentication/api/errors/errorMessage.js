// ----------------------------------------------------------------
// QUERY HANDLER --------------------------------------------------
const emailAlreadyUsed = 'This email is already used';
const nonExistentUser = 'This user doesn\'t exist';
const noUserCreated = 'No user has been created';
const noUserUpdated = 'No user has been updated';
const noUserFound = 'No user has been found';
const noUserRemoved = 'No user has been removed';

const badCredentials = 'Bad credentials';

// ----------------------------------------------------------------
// 400 - BAD REQUEST ----------------------------------------------

const userIdMissingInUrl = 'No user ID provided in the url';
const requestInformationMissing = 'Request format not respected';

// ----------------------------------------------------------------
// 401 - UNAUTHORIZED ---------------------------------------------

const tokenIsIncomplete = 'Token not complete';
const tokenIsMissing = 'No token provided';
const tokenIsInvalid = 'The token provided isn\'t valid';

// ----------------------------------------------------------------
// 403 - FORBIDDEN ------------------------------------------------
const notAnAdmin = 'Only an administrator can do this';

module.exports = {
  emailAlreadyUsed,
  nonExistentUser,
  noUserCreated,
  noUserUpdated,
  noUserFound,
  noUserRemoved,
  badCredentials,
  userIdMissingInUrl,
  requestInformationMissing,
  tokenIsIncomplete,
  tokenIsMissing,
  tokenIsInvalid,
  notAnAdmin,
};
