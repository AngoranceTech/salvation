/* global User */
/* eslint no-console: 0 */

const bcrypt = require('bcrypt');
const errorHandler = require('../errors/errorHandler');
const errorMessage = require('../errors/errorMessage');

const db = User.getDatastore().manager.collection('user');

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * Retrieve the ID linked to a specific email
 * @param {*} email Email of the ID to retrieve
 */
function getIdByEmail(email) {
  return db.findOne({ email }, { salvationId: 1 })
    .then((response) => {
      // Name has been found
      if (response != null) {
        console.log(`ID ${response.salvationId} retrieved`);
        return response.salvationId;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noUserFound);
    });
}

/**
 * Create one user in the database
 * @param {*} user User to create
 */
function insertEntry(user = {}) {
  return db.insertOne(user)
    .then((operation) => {
      // User has been created
      if (operation.insertedCount === 1) {
        console.log('User created');
        return { id: user.salvationId, email: user.email, role: user.role };
      }
      throw new errorHandler.SpecialError(500, errorMessage.noUserCreated);
    });
}

/**
 * Create one user in the database by defining special id
 * @param {*} email
 * @param {*} password
 * @param {*} role
 */
function insertUser(email, password, role) {
  return bcrypt.hash(password, 5)
    .then((hash) => {
      console.log(`Hash generated: ${hash}`);

      return db.find().sort({ salvationId: -1 }).limit(1)
        .toArray()
        .then((doc) => {
          // This user isn't the first one
          if (doc.length > 0) {
            const id = doc[0].salvationId + 1;
            console.log(`New ID: ${id}`);

            // Insert the user
            return insertEntry({ salvationId: id, email, password: hash, role })
              .then(result => result);
          }

          // Insert the first user
          console.log('No ID max found, starting at 0.');
          return insertEntry({ salvationId: 0, email, password: hash, role })
            .then(result => result);
        });
    });
}

/**
 * Get all users from the database
 */
function selectUsers() {
  return db.find({}, { _id: 0, salvationId: 1, email: 1, role: 1 }).toArray()
    .then((users) => {
      console.log('Users retrieved');
      return users;
    });
}

/**
 * Update user's email in the database
 * @param {*} id Id of the user to update
 * @param {*} email Email to update
 * @param {*} role Role to update
 */
function updateUser(id, email, role) {
  // Hash the password, before trying to update the user
  return db.updateOne({ salvationId: parseInt(id) }, { $set: { email, role } })
    .then((operation) => {
      // User has been updated
      if (operation.result.n === 1) {
        console.log('User updated');
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noUserUpdated);
    });
}

/**
 * Update user's password in the database
 * @param {*} id Id of the user to update
 * @param {*} password User to update
 */
function updatePasswordUser(id, password) {
  // Hash the password, before trying to update the user
  return bcrypt.hash(password, 5)
    .then((hash) => {
      console.log(`Hash generated: ${hash}`);

      return db.updateOne({ salvationId: id }, { $set: { password: hash } })
        .then((operation) => {
          // User has been updated
          if (operation.result.n === 1) {
            console.log('Password updated');
            return;
          }
          throw new errorHandler.SpecialError(500, errorMessage.noUserUpdated);
        });
    });
}

/**
 * Delete one user from the database
 * @param {*} id Id of the user to delete
 */
function deleteUser(id) {
  return db.removeOne({ salvationId: id })
    .then((operation) => {
      // User has been removed
      if (operation.result.n === 1) {
        console.log('User removed');
        return;
      }
      throw new errorHandler.SpecialError(500, errorMessage.noUserRemoved);
    });
}

// ----------------------------------------------------------------
// Login OPERATIONS -----------------------------------------------

/**
 * Login a user
 * @param {*} email
 * @param {*} password
 */
function loginUser(email, password) {
  return db.findOne({ email }, { fields: { salvationId: 1, password: 1, role: 1 } })
    .then((user) => {
      if (user != null) {
        console.log(`User found: { id: ${user.salvationId}, email: ${email}, password: ${user.password}, role: ${user.role} }`);

        // Password comparison
        return bcrypt.compare(password, user.password)
          .then((result) => {
            // Login passed
            if (result === true) {
              return { salvationId: user.salvationId, role: user.role };
            }
            throw new errorHandler.SpecialError(401, errorMessage.badCredentials);
          });
      }
      throw new errorHandler.SpecialError(401, errorMessage.nonExistentUser);
    });
}

// ----------------------------------------------------------------
// Accounts OPERATIONS --------------------------------------------

/**
 * Get one user from the database
 * @param {*} id ID of the user to retrieve
 */
function selectUser(id) {
  return db.findOne({ salvationId: id }, { _id: 0, salvationId: 1, email: 1, role: 1 })
    .then((user) => {
      if (user != null) {
        console.log('User retrieved');
        return user;
      }
      throw new errorHandler.SpecialError(400, errorMessage.noUserFound);
    });
}

module.exports = {
  getIdByEmail,
  selectUsers,
  loginUser,
  insertUser,
  updateUser,
  updatePasswordUser,
  deleteUser,
  selectUser,
};
