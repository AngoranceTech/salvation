/**
 * ConnectionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

/* global sails */
/* eslint no-undef: "error" */
/* eslint no-console: 0 */

const jwt = require('jsonwebtoken');
const queries = require('../queries/queryHandler');
const errorHandler = require('../errors/errorHandler');

const { secret } = sails.config.authenticationConf;

// ----------------------------------------------------------------
// SEND RESPONSE --------------------------------------------------
function sendResponse(res, result) {
  res.status(200);

  if (result !== undefined) {
    res.json(result);
  } else {
    res.send();
  }
}

// ----------------------------------------------------------------
// SEND SPECIAL ERROR ---------------------------------------------
function sendSpecialError(err, res) {
  if (err instanceof errorHandler.SpecialError) {
    errorHandler.sendError(err.err, res, err.code);
  } else {
    errorHandler.sendError(err, res, 0);
  }
}

// ----------------------------------------------------------------
// AUTHORIZATION VALIDATION ---------------------------------------
function validAuthorization(authorization, res) {
  if (authorization !== undefined) {
    try {
      const token = jwt.verify(authorization, secret);

      if (token.salvationId !== undefined && token.role !== undefined) {
        return token;
      }
      errorHandler.tokenIsIncomplete(res);
      return undefined;

    } catch (err) {
      errorHandler.tokenIsInvalid(res);
      return undefined;
    }
  }

  errorHandler.tokenIsMissing(res);
  return undefined;
}

// ----------------------------------------------------------------
// Admin OPERATIONS -----------------------------------------------

/**
 * [POST]
 * Create a new user account
 *
 * @param {*} req header(authorization), body(credentials)
 * @param {*} res json(id, email, role) and code 200, an error otherwise
 */
function createUser(req, res) {
  console.log('CREATE_USER: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const credentials = req.body;
  const email = credentials.email;
  const password = credentials.password;
  const role = credentials.role;

  // Check information
  if (token !== undefined) {
    // Only the administrator can do this operation
    if (token.role === 'admin') {
      if (email !== undefined && password !== undefined && role !== undefined) {
        // Create the user in the database
        queries.insertUser(email, password, role)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.requestInformationMissing(res);
      }
    } else {
      errorHandler.notAnAdmin(res);
    }
  }
}

/**
 * [GET]
 * Get list of created accounts
 *
 * @param {*} req headers(authorization)
 * @param {*} res json([accounts]) and code 200, an error otherwise
 */
function getUsers(req, res) {
  console.log('LIST_USERS: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  if (token !== undefined) {
    // Only the administrator can do this operation
    if (token.role === 'admin') {
      // Get all users from the database
      queries.selectUsers()
        .then(result => sendResponse(res, result))
        .catch(err => sendSpecialError(err, res));
    } else {
      errorHandler.notAnAdmin(res);
    }
  }
}

/**
 * [PATCH]
 * Modify the user's account
 *
 * An administrator can modify only the email and the role of the account
 * Another role can modify only the password of the account
 *
 * @param {*} req params(userId), header(authorization), body(credentials)
 * @param {*} res code 200, an error otherwise
 */
function updateUser(req, res) {
  console.log('UPDATE_USER: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Request information
  const credentials = req.body;
  const email = credentials.email;
  const password = credentials.password;
  const role = credentials.role;

  // Check information
  const userId = parseInt(req.params.userId, 10);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Admin can modify only the email and the role
      if (token.role === 'admin' && token.salvationId !== userId) {
        if (email !== undefined && role !== undefined) {
          // Update the user in the database
          queries.updateUser(userId, email, role)
            .then(result => sendResponse(res, result))
            .catch(err => sendSpecialError(err, res));
        } else {
          errorHandler.requestInformationMissing(res);
        }
      } else if (token.salvationId === userId && email === undefined && role === undefined) {
        // Other roles can modify only the password of their own account
        if (password !== undefined) {
          // Update the user's password in the database
          queries.updatePasswordUser(userId, password)
            .then(result => sendResponse(res, result))
            .catch(err => sendSpecialError(err, res));
        } else {
          errorHandler.requestInformationMissing(res);
        }
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

/**
 * [DELETE]
 * Delete a user account
 *
 * @param {*} req params(userId), headers(authorization)
 * @param {*} res code 204, an error otherwise
 */
function removeUser(req, res) {
  console.log('REMOVE_USER: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Only the administrator can do this operation
      if (token.role === 'admin') {
        // Remove the user from the database
        queries.deleteUser(userId)
          .then(response => res.json(response))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

// ----------------------------------------------------------------
// Login OPERATIONS -----------------------------------------------

/**
 * [POST]
 * Try to login an user and return jwt
 *
 * @param {*} req body(credentials)
 * @param {*} res json(jwt) and code 200, an error otherwise
 */
function login(req, res) {
  console.log('LOGIN: ');

  // Request information
  const credentials = req.body;
  const email = credentials.email;
  const password = credentials.password;

  // Check information
  if (email !== undefined && password !== undefined) {
    // Check if the user exists in the database
    queries.loginUser(email, password)
      .then((result) => {
        // Create token
        const token = jwt.sign({ salvationId: result.salvationId, role: result.role }, secret, {
          expiresIn: '1h',
        });
        sendResponse(res, { jwt: token, salvationId: result.salvationId });
      })
      .catch(err => sendSpecialError(err, res));
  } else {
    errorHandler.requestInformationMissing(res);
  }
}

// ----------------------------------------------------------------
// Accounts OPERATIONS --------------------------------------------

/**
 * [GET]
 * Get user account
 *
 * @param {*} req params(userId), headers(authorization)
 * @param {*} res json(id, email, role) and code 200 if ok, an error otherwise
 */
function getUser(req, res) {
  console.log('GET_USER: ');

  // Authorization validation
  const authorization = req.header('Authorization');
  const token = validAuthorization(authorization, res);

  // Check information
  const userId = parseInt(req.params.userId);

  if (token !== undefined) {
    if (userId !== undefined) {
      // Only the administrator or the concerned user can do this operation
      if (token.role === 'admin' || token.salvationId === userId) {
        // Get the account from the database
        queries.selectUser(userId)
          .then(result => sendResponse(res, result))
          .catch(err => sendSpecialError(err, res));
      } else {
        errorHandler.notAnAdmin(res);
      }
    } else {
      errorHandler.userIdMissingInUrl(res);
    }
  }
}

/**
 * Retrieve the ID of a user with his email
 * @param {*} req 
 * @param {*} res 
 */
function getIdByEmail(req, res) {
  console.log('GET_ID_BY_EMAIL: ');

  // Check information
  const email = req.query.email;

  if (email !== undefined) {
    // Get ID from the database
    queries.getIdByEmail(email)
      .then(result => sendResponse(res, result))
      .catch(err => sendSpecialError(err, res));

  } else {
    errorHandler.requestInformationMissing(res);
  }
}

module.exports = {
  createUser,
  getUsers,
  updateUser,
  removeUser,
  login,
  getUser,
  getIdByEmail,
};
