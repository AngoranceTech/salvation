/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function (done) {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return done();
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  // Don't forget to trigger `done()` when this bootstrap function's logic is finished.
  // (otherwise your server will never lift, since it's waiting on the bootstrap)

  // sails.on(eventName, evenHandlerFn);
  // lifted : The app has been lifted and is listening for requests.
  sails.on('lifted', function () {
    let db = User.getDatastore().manager.collection('user');

    //Create unique indexes
    db.createIndex({ "email": 1 }, { unique: true });

    // Create a default admin account (pass: P@ssw0rd)
    db.insertOne({ salvationId: 0, email: 'admin@salvation.ch', password: '$2b$05$YZia1ISS1LfwjawjizKf.Oi9WO2uvm1sbaWD85FmXyRZZU74WRvKS', role: 'admin'});
  
    // Create a default student account for tests (pass: P@ssw0rd)
    db.insertOne({ salvationId: 1, email: 'test@salvation.ch', password: '$2b$05$YZia1ISS1LfwjawjizKf.Oi9WO2uvm1sbaWD85FmXyRZZU74WRvKS', role: 'student'});

    // Create a default service account for cafeteria (pass: P@ssw0rd)
    db.insertOne({ salvationId: 2, email: 'cafeteria@salvation.ch', password: '$2b$05$YZia1ISS1LfwjawjizKf.Oi9WO2uvm1sbaWD85FmXyRZZU74WRvKS', role: 'service'});

    // Create a default service account for parking (pass: P@ssw0rd)
    db.insertOne({ salvationId: 3, email: 'parking@salvation.ch', password: '$2b$05$YZia1ISS1LfwjawjizKf.Oi9WO2uvm1sbaWD85FmXyRZZU74WRvKS', role: 'service'});
  
    // Create a default service account for printer (pass: P@ssw0rd)
    db.insertOne({ salvationId: 4, email: 'printer@salvation.ch', password: '$2b$05$YZia1ISS1LfwjawjizKf.Oi9WO2uvm1sbaWD85FmXyRZZU74WRvKS', role: 'service'});
  });

  return done();

};
