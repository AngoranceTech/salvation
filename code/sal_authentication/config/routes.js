/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /**
   * Services
   */
  'GET      /idByEmail': 'UserController.getIdByEmail',

  /**
   * Admin
   *
   * Entrypoints for administration. Allows to create accounts and list them all. 
   * Allow to modify accounts and delete them too.
   */
  'POST     /registration': 'UserController.createUser',
  'GET      /accounts': 'UserController.getUsers',
  'PATCH    /accounts/:userId': 'UserController.updateUser',
  'DELETE   /accounts/:userId': 'UserController.removeUser',

  /**
   * Login
   *
   * Allows users to login and get their JWT.
   */
  'POST      /authentication': 'UserController.login',

  /**
   * Account
   *
   * Allows users to modify their password and get their data.
   */
  'GET      /accounts/:userId': 'UserController.getUser',
};
