const { expect } = require('chai');
const sails = require('sails');
const axios = require('axios');
const errorMessage = require('../api/errors/errorMessage');

const client = axios.create({
  baseURL: 'http://localhost:1337',
  timeout: 2000,
});

describe('AUHTHENTICATION SERVICE', function () {

  /* ========================================================================
  /*  LOGIN (role admin)
  /*====================================================================== */

  let token = "";

  it('should say that a field is missing', function () {
    return client
      .post('/authentication', { password: 'P@ssw0rd' })
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.requestInformationMissing);
      });
  });

  it('should say that the user doesn\' exist', function () {
    return client
      .post('/authentication', { email: 'unknown@salvation.ch', password: 'Passw0rd' })
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.nonExistentUser);
      });
  });

  it('should say that bad credentials have been sent', function () {
    return client
      .post('/authentication', { email: 'admin@salvation.ch', password: 'Passw0rd' })
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.badCredentials);
      });
  });

  it('should log in the administrator', function () {
    return client
      .post('/authentication', { email: 'admin@salvation.ch', password: 'P@ssw0rd' })
      .then((response) => {

        // Get the token
        token = response.data.jwt;
        salvationId = response.data.salvationId;

        const expected = { jwt: token, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  USER CREATION (role admin)
  /*====================================================================== */

  let userId = -1;

  it('should say that the token is missing', function () {
    const data = { email: 'student1@salvation.ch', password: 'monPass', role: 'student' };

    return client
      .post('/registration', data)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should say that a field is missing in the account provided (role admin)', function () {
    const data = { email: 'student1@salvation.ch', role: 'student' };
    const header = { headers: { Authorization: token } };

    return client
      .post('/registration', data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.requestInformationMissing);
      });
  });

  it('should create a user student1 (role admin)', function () {
    const data = { email: 'student1@salvation.ch', password: 'monPass', role: 'student' };
    const header = { headers: { Authorization: token } };

    return client
      .post('/registration', data, header)
      .then((response) => {
        userId = response.data.id;
        const expected = { id: userId, email: 'student1@salvation.ch', role: 'student' };
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  USER'S ID RETRIEVAL BY EMAIL
  /*====================================================================== */

  it('should retrieve the ID of the user\'s email', function () {

    return client
      .get('/idByEmail?email=student1@salvation.ch')
      .then((response) => {
        const expected = userId;
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  USERS RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get('/accounts')
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve 6 users (1 admin, 2 students, 3 services) (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get('/accounts', header)
      .then((response) => {
        const expected = [
          {
            salvationId: 0,
            email: 'admin@salvation.ch',
            role: 'admin'
          },
          {
            salvationId: 1,
            email: 'test@salvation.ch',
            role: 'student'
          },
          {
            salvationId: 2,
            email: 'cafeteria@salvation.ch',
            role: 'service'
          },
          {
            salvationId: 3,
            email: 'parking@salvation.ch',
            role: 'service'
          },
          {
            salvationId: 4,
            email: 'printer@salvation.ch',
            role: 'service'
          },
          {
            salvationId: userId,
            email: 'student1@salvation.ch',
            role: 'student'
          }
        ];
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  USER UPDATE (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .patch(`/accounts/${userId}`, { email: 'student1Modified@salvation.ch', role: 'student' })
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should say that no email has been provided (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .patch(`/accounts/${userId}`, { role: 'student' }, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.requestInformationMissing);
      });
  });

  it('should update the email of user student1 (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .patch(`/accounts/${userId}`, { email: 'student1Modified@salvation.ch', role: 'student' }, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  USER RETRIEVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .get(`/accounts/${userId}`)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should retrieve user student1 (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .get(`/accounts/${userId}`, header)
      .then((response) => {
        const expected = {
          salvationId: userId,
          email: 'student1Modified@salvation.ch',
          role: 'student'
        };
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  LOGIN (role student)
  /*====================================================================== */

  let tokenUser = "";

  it('should log in student1', function () {
    return client
      .post('/authentication', { email: 'student1Modified@salvation.ch', password: 'monPass' })
      .then((response) => {

        // Get the token
        tokenUser = response.data.jwt;
        salvationId = response.data.salvationId;

        const expected = { jwt: tokenUser, salvationId: salvationId };
        expect(response.data).to.eql(expected);
      });
  });

  /* ========================================================================
  /*  USERS RETRIEVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .get('/accounts', header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  USER CREATION (role student)
  /*====================================================================== */
 
  it('should say that only an administrator can do this (role student)', function () {
    const data = { email: 'student2@salvation.ch', password: 'monPass', role: 'student' };
    const header = { headers: { Authorization: tokenUser } };

    return client
      .post('/registration', data, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  USER UPDATE (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .patch(`/accounts/${userId}`, { email: 'student1Modified2@salvation.ch', password: 'pass' }, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .patch(`/accounts/${userId + 1}`, { password: 'myModifiedPass' }, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  it('should say that the password is missing (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .patch(`/accounts/${userId}`, {}, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.requestInformationMissing);
      });
  });

  it('should update the password of user student1Modified (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .patch(`/accounts/${userId}`, { password: 'myModifiedPass' }, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });

  /* ========================================================================
  /*  USER REMOVAL (role student)
  /*====================================================================== */

  it('should say that only an administrator can do this (role student)', function () {
    const header = { headers: { Authorization: tokenUser } };

    return client
      .delete(`/accounts/${userId}`, header)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.notAnAdmin);
      });
  });

  /* ========================================================================
  /*  USER REMOVAL (role admin)
  /*====================================================================== */

  it('should say that the token is missing', function () {
    return client
      .delete(`/accounts/${userId}`)
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.data).to.eql(errorMessage.tokenIsMissing);
      });
  });

  it('should remove student1 (role admin)', function () {
    const header = { headers: { Authorization: token } };

    return client
      .delete(`/accounts/${userId}`, header)
      .then((response) => {
        expect(response.status).to.eql(200);
      });
  });
});