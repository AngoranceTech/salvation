
Feature('Basic scenario');

Scenario('basic working test', async (I) => {
	I.amOnPage('/Auth');
	I.seeCurrentUrlEquals('/Auth');
	I.see('Log in');

	/**
	 * All Tests with admin account, creation : update and delete and put money on wallet
	 * and some test with normal account : change password and transfert money to another user
	 */

	//Connection admin account
	I.fillField('email', 'admin@salvation.ch');
	I.fillField('password', 'P@ssw0rd');

	I.pressKey('Enter');
	I.seeCurrentUrlEquals('/Nav/Profile');
	I.see('Salvation')

	/**
	 * Test each page
	 */


	//Profile Page
	I.click('menu');
	I.click('Profile');
	I.seeCurrentUrlEquals('/Nav/Profile');

	//Wallet Page
	I.click('Wallet');
	I.seeCurrentUrlEquals('/Nav/Wallet');

	//Admin Page
	I.click('Admin');
	I.seeCurrentUrlEquals('/Nav/Admin/Users');

	//Create User
	I.click('add');

	I.fillField('First Name', 'Romflex');
	I.fillField('Last Name', 'Ample');
	I.fillField('birthdate', '06/10/1993');
	I.fillField('Email', 'romflex.ample@heig-vd.ch');
	I.fillField('Phone Number', '0123456789');
	I.fillField('Address', 'No Address');
	I.fillField('NPA', '6666');
	I.fillField('City', 'No city');
	I.click('Role');
	I.click({id:'Student'});
	I.click('check');
	I.see('Success creation user')
	I.click('add');
	I.click('clear');

	//Edit user
	I.click('Admin');
	I.seeCurrentUrlEquals('/Nav/Admin/Users');
	I.fillField('Recherche', 'romflex.ample@heig-vd.ch');
	I.click('edit');

	let url = await I.grabCurrentUrl();
	let tab = url.split('/');
	I.seeCurrentUrlEquals('/Nav/Admin/Users/' + tab[6]);

	I.click({ name: 'update_profile' });
	I.see('Phone number');
	I.fillField('Phone number', '0791231212');
	I.click('check');
	I.see('Success update');
	I.click({name:'update_wallets'});
	I.see('Amount');
	I.fillField('Amount','20');
	I.click('Add');
	I.see("Top up : Success")
	I.click('Users');
	I.seeCurrentUrlEquals('/Nav/Admin/Users');
	I.fillField('Recherche', 'romflex.ample@heig-vd.ch');
	I.see('0791231212');

	//Connexion new user
	I.click('Log out');
	I.seeCurrentUrlEquals('/Auth');
	I.see('Log in');

	I.fillField('email', 'romflex.ample@heig-vd.ch');
	I.fillField('password', 'password123');

	I.pressKey('Enter');
	I.seeCurrentUrlEquals('/Nav/Profile');
	I.click('menu');
	I.click('Profile');

	//Changement de mot de passe
	I.click('Change password');
	I.fillField('Password', 'password');
	I.fillField('Password Confirmation', 'password');
	I.click('check');
	I.click('Change password');
	I.click('clear');

	//Connexion avec le nouveau mot de passe
	I.click('Log out');
	I.seeCurrentUrlEquals('/Auth');
	I.see('Log in');

	I.fillField('email', 'romflex.ample@heig-vd.ch');
	I.fillField('password', 'password');

	I.pressKey('Enter');
	I.seeCurrentUrlEquals('/Nav/Profile');
	I.click('menu');
	I.click('Wallet');
	I.see('70.-');

	//Transfert d'argent utilisater à utilisateur
	I.click('Lend money');
	I.fillField('Amount','18');
	I.fillField('Email','admin@salvation.ch');
	I.fillField('Description','Transfert d\'argent');
	I.click('check');
	I.see('Success lend money')
	I.see('-18');
	I.click('menu');
	I.click('menu');


	//Delete User
	I.click('Log out');

	I.seeCurrentUrlEquals('/Auth');
	I.see('Log in');

	I.fillField('email', 'admin@salvation.ch');
	I.fillField('password', 'P@ssw0rd');

	I.pressKey('Enter');
	I.seeCurrentUrlEquals('/Nav/Profile');

	I.click('menu');
	I.click('Admin');
	I.seeCurrentUrlEquals('/Nav/Admin/Users');
	I.fillField('Recherche', 'romflex.ample@heig-vd.ch');
	I.click('clear');
	I.see('Voulez-vous vraiment supprimer cet utilisateur ?');
	I.click('Non');
	I.click('clear');
	I.see('Voulez-vous vraiment supprimer cet utilisateur ?');
	I.click('Oui');
	I.fillField('Recherche', 'romflex.ample@heig-vd.ch');
	I.dontSee('romflex.ample@heig-vd.ch');
});

Scenario('basic failing test', async (I) => {
	I.amOnPage('/Auth');
	I.seeCurrentUrlEquals('/Auth');
	I.see('Log in')

	//Bad credential
	I.fillField('email', 'admin@salvation.ch');
	I.fillField('password', '123456');

	I.pressKey('Enter');
	I.seeCurrentUrlEquals('/Auth');
	I.see('Bad credentials');

	//User doesn't exist
	I.seeCurrentUrlEquals('/Auth');
	I.see('Log in')

	I.fillField('email', 'jean.dupon@heig-vd.ch');
	I.fillField('password', '123456');

	I.pressKey('Enter');
	I.seeCurrentUrlEquals('/Auth');
	I.see('This user doesn\'t exist');
});