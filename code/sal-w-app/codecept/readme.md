# Codecept - Test

Pour lancer les testes de la web app il faut tout d'abors installer codecept. Aller dans le dossier de codecept dans `code\sal-w-app\codecept` et faire la commande suivante

```
npm install
```

Maintenant que codecept est installé, on peut lancer la commande suivante pour lancer les testes

```
codeceptjs run tests/salvation_test.js --steps
```

Si il y a eu une erreur lors du teste, un screenshot sera mis dans output pour montrer ou il y a eu un problème durant les testes. Et dans le terminal cela nous montrera a quel étape le teste a échoué grace à `--steps`

