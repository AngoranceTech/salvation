import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTabGroup } from '@angular/material';
import { WalletStructure } from 'src/app/interfaces/IWalletStructure';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { TransactionStructure } from 'src/app/interfaces/ITransactionStructure';
import { ProfileStructure } from '../navigator.component';
import { isUndefined } from 'util';

@Component({
  selector: 'app-wallet-page',
  templateUrl: './wallet-page.component.html',
  styleUrls: ['./wallet-page.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class WalletPageComponent implements OnInit {

  displayedColumns: string[] = ['amount', 'from', 'to', 'description', 'date'];
  TRANSACTION_MAIN_DATA: TransactionStructure[];
  TRANSACTION_PRINTER_DATA: TransactionStructure[]

  dataSourcePrinter;
  dataSourceMain;

  @ViewChild('sortPrinter') sortPrinter: MatSort;
  @ViewChild('sortMain') sortMain: MatSort;

  _srvURLWallet = 'https://salvation.angorance.tech/api/wallet';

  _pageNumber = 1;
  _pageNumberMain = 1;
  _pageNumberPrinter = 1;

  _WalletIdMain: string;
  _WalletIdPrint: string;

  _index = 0;

  wallets: WalletStructure[];

  constructor(
    private http: HttpClient,
  ) {
    this.getWallets();
  }

  ngOnInit() {
  }



  _setDataSource(indexNumber) {
    this._index = indexNumber;
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          !this.dataSourceMain.sort ? this.dataSourceMain.sort = this.sortMain : null;
          break;
        case 1:
          !this.dataSourcePrinter.sort ? this.dataSourcePrinter.sort = this.sortPrinter : null;
      }
    });
  }

  getWallets() {
    const useURL = this._srvURLWallet + "/wallets/";
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.get(useURL, httpOption).subscribe((res: WalletStructure[]) => {
      this.wallets = res;
      this.doingTransactionRequest();
      console.log("Wallet : ", "Success get wallets");
    }, (error) => {
      console.log("Wallet : ", "Fail get wallets");
      console.log("Wallet : ", error);
    });

  }

  getTransactions(walletId: string) {
    const useURL = this._srvURLWallet + "/wallets/" + walletId + "/transactions";
    let token = sessionStorage.getItem('token');
    const parameters = new HttpParams().set('pageNumber', this._pageNumber.toString());
    const httpOption = {
      params: parameters,
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.get(useURL, httpOption).subscribe((res: TransactionStructure[]) => {
      this.setTransactionData(walletId, res);
      console.log("Wallet transaction : ", "Success get transaction");
    }, (error) => {
      console.log("Wallet transaction : ", "Fail get transaction");
      console.log("Wallet transaction : ", error);
    });

  }

  doingTransactionRequest() {
    for (let wallet of this.wallets) {
      this.getTransactions(wallet._id)
    }
  }
  setTransactionData(walletId: string, res: TransactionStructure[]) {

    for (let wallet of this.wallets) {
      if (wallet._id == walletId) {
        if (wallet.type == "main") {
          this._WalletIdMain = wallet._id;
          this.TRANSACTION_MAIN_DATA = res;
          this.checkAmount(this.TRANSACTION_MAIN_DATA);
          this.dataSourceMain = new MatTableDataSource(this.TRANSACTION_MAIN_DATA);
        } else if (wallet.type == "print") {
          this._WalletIdPrint = wallet._id;
          this.TRANSACTION_PRINTER_DATA = res;
          this.checkAmount(this.TRANSACTION_PRINTER_DATA);
          this.dataSourcePrinter = new MatTableDataSource(this.TRANSACTION_PRINTER_DATA);
        }
      }
    }
  }
  
  checkAmount(transactions:TransactionStructure[]){
    let salvationId = sessionStorage.getItem('salvationId');
    for(let transaction of transactions){
      if(transaction.from.salvationId === Number(salvationId)){
        transaction.amount *= -1;
      }
    }
  }

  nextPage() {
    switch (this._index) {
      case 0:
        this._pageNumberMain++;
        this._pageNumber = this._pageNumberMain;
        this.getTransactions(this._WalletIdMain);
        break;
      case 1:
        console.log("nextPage", this._index)
        this._pageNumberPrinter++;
        this._pageNumber = this._pageNumberPrinter;
        this.getTransactions(this._WalletIdPrint);
    }
  }

  previousPage() {
    switch (this._index) {
      case 0:
        if (this._pageNumberMain > 1) {
          this._pageNumberMain--;
          this._pageNumber = this._pageNumberMain;
          this.getTransactions(this._WalletIdMain);
        }
        break;
      case 1:
        if (this._pageNumberPrinter > 1) {
          this._pageNumberPrinter--;
          this._pageNumber = this._pageNumberPrinter;
          this.getTransactions(this._WalletIdPrint);
        }
    }
  }

}
