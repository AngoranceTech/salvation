import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { WalletPageComponent } from '../wallet-page.component';
import { FormControl, Validators } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ProfileStructure } from '../../navigator.component';
import { isUndefined } from 'util';

@Component({
  selector: 'app-dialog-transaction',
  templateUrl: './dialog-transaction.component.html',
  styleUrls: ['./dialog-transaction.component.css']
})
export class DialogTransactionComponent implements OnInit {
  _srvURLWallet = 'https://salvation.angorance.tech/api/wallet';
  constructor(
    public dialogRef: MatDialogRef<WalletPageComponent>,
    private http: HttpClient,
    private router: Router,
    public snackBar: MatSnackBar,) { }

  errorMessage : String;
  amount = new FormControl('', [Validators.required]);
  email_to = new FormControl('', [Validators.required, Validators.email]);
  description = new FormControl('', [Validators.required]);
  profile : ProfileStructure;


  getErrorMessage() {
    return this.email_to.hasError('required') ? 'You must enter a value' :
      this.email_to.hasError('email') ? 'Not a valid email' : '';
  }

  openNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  transactionUserToUser() {
    const useURL = this._srvURLWallet + "/transactions";
    let token = sessionStorage.getItem('token');
    let salvationId = sessionStorage.getItem('salvationId');
    this.profile = JSON.parse(sessionStorage.getItem('profile'));
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };
    let name = (isUndefined(this.profile.last_name) ? "" : this.profile.last_name) + " " + (isUndefined(this.profile.first_name) ? "" : this.profile.first_name);

    let transactionBody = {
      "amount": this.amount.value,
      "from": {
        "salvationId": parseFloat(salvationId),
        "name": name,
      },
      "to": {
        "email": this.email_to.value,
      },
      "description": this.description.value,
      "type": "main",
    }
    
    if(this.validateform()){
      this.http.post(useURL, transactionBody,httpOption).subscribe((res) => {
        console.log("Transaction : ", "Success creation");
        this.router.navigateByUrl('/Nav', { skipLocationChange: false }).then(() => {
          this.router.navigate(['/Nav/Wallet'])
        })
        this.dialogRef.close();
        this.openNotification('Success lend money',null);
      }, (error) => {
        console.log("Transaction : ", "Fail creation");
        console.log("Transaction : ", error);
        this.errorMessage = error.error;
        this.openNotification('Fail lend money',null);
      });
    }else{
      this.errorMessage = "Veuillez remplir tout les champs"
    }

  }

  validateform(): boolean {

    return this.email_to.valid &&
      this.description.valid &&
      this.amount.valid;
  }

  noAction() {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
