import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { DialogTransactionComponent } from '../dialog-transaction/dialog-transaction.component';

@Component({
  selector: 'app-button-transaction',
  templateUrl: './button-transaction.component.html',
  styleUrls: ['./button-transaction.component.css']
})
export class ButtonTransactionComponent implements OnInit {

  dialogTransaction:MatDialogRef<DialogTransactionComponent>;

  constructor(
    public dialog: MatDialog,) { }

  ngOnInit() {
  }

  openDialog() {
    console.log('opening user creation dialog');

    this.dialogTransaction = this.dialog.open(DialogTransactionComponent, {
      width: '20%',
      minWidth: '200px',
      autoFocus: false,
    });

    this.dialogTransaction.afterClosed().subscribe(() => {
      console.log('user creation dialog closed')
    });

  }

}
