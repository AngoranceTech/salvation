import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigatorComponent } from './navigator.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { WalletPageComponent } from './wallet-page/wallet-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { AccessPageComponent } from './access-page/access-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { DoorsComponent } from './admin-page/doors/doors.component';
import { GroupsComponent } from './admin-page/groups/groups.component';
import { UserDetailsComponent } from './admin-page/user-details/user-details.component';
import { UserListComponent } from './admin-page/user-list/user-list.component';

const routes: Routes = [
  { 
    path : 'Nav', 
    component: NavigatorComponent,
    children : [
      { path:'', redirectTo: 'Profile', pathMatch:'full' },
      { path: 'Dashboard', component: DashboardPageComponent },
      { path: 'Wallet', component: WalletPageComponent },
      { path: 'Profile', component: ProfilePageComponent },
      { path: 'Access', component: AccessPageComponent },
      { path: 'Admin', 
        component: AdminPageComponent,
        children :[
          { path: 'Users', component: UserListComponent },
          { path: 'Users/:id', component: UserDetailsComponent },
          { path: 'Doors', component: DoorsComponent },
          { path: 'Groups', component: GroupsComponent }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavigatorRoutingModule { }
