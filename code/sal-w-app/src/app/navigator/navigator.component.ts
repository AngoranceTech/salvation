import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { IToken } from '../interfaces/IToken';
import * as jwt_decode from "jwt-decode";

export interface ProfileStructure {
  first_name: string,
  last_name: string,
}

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.css']
})
export class NavigatorComponent implements OnInit {

  username;
  token : IToken;

  _srvURL = 'https://salvation.angorance.tech/api'; // '/prof/profile?token='
  _servicesURL: Array<Service> = [
    { name: 'profile', URL: '/prof/profiles/' },
  ]
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    if (sessionStorage.getItem('token') === null) {
      this.router.navigateByUrl('/Auth');
    }


    // récupération des données de chacun des services
    this.getServiceData(this._servicesURL[0]); // TODO <-- uncomment this line once all is adapted
  }

  getServiceData(targetService: Service) {
    let sToken = sessionStorage.getItem('token');
    const httpOption = { 
        headers: new HttpHeaders({
        'Authorization' : sToken
      })
    };

    this.token = this.getDecodedAccessToken(sToken);

    const useURL = this._srvURL + targetService.URL + this.token.salvationId;

    console.log(useURL);
    this.http.get(useURL, httpOption).subscribe((res) => {
      sessionStorage.setItem(targetService.name, JSON.stringify(res));
      /*if (res.status) {
        this.username = res.result.profile.first_name + ' ' + res.result.profile.last_name;
      }*/
    }, (error) => {
      console.log(error);
    })
  }

  logout() {
    sessionStorage.removeItem('token');
    this.router.navigateByUrl('/Auth');
  }

  getDecodedAccessToken(token: string): IToken {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

  isAdmin(){
    return this.token !== null && this.token.role.toLowerCase() === 'admin';
  }

  sideNavOpened: boolean = false;
}

class Service {
  name: string;
  URL: string;
}