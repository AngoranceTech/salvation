import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatSidenavModule,
  MatIconModule,
  MatAutocompleteModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatSnackBarModule
} from '@angular/material';

import { NavigatorRoutingModule } from './navigator-routing.module';

import { ProfilePageComponent, PasswordDialog } from './profile-page/profile-page.component';
import { WalletPageComponent } from './wallet-page/wallet-page.component';
import { AccessPageComponent } from './access-page/access-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { NavigatorComponent } from './navigator.component';
import { AdminPageModule } from './admin-page/admin-page.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogTransactionComponent } from './wallet-page/dialog-transaction/dialog-transaction.component';
import { ButtonTransactionComponent } from './wallet-page/button-transaction/button-transaction.component';


@NgModule({
  imports: [
    CommonModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatSnackBarModule,
    NavigatorRoutingModule,
    AdminPageModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  declarations: [
    ProfilePageComponent,
    WalletPageComponent,
    AccessPageComponent,
    DashboardPageComponent,
    PasswordDialog,
    NavigatorComponent,
    DialogTransactionComponent,
    ButtonTransactionComponent,
  ],
  entryComponents:[PasswordDialog]
})
export class NavigatorModule { }
