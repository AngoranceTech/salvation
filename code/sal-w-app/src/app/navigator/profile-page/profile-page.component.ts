import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { IToken } from '../../interfaces/IToken';
import * as jwt_decode from "jwt-decode";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Validators, FormControl } from '@angular/forms';
import { ProfileStructure } from 'src/app/interfaces/IProfileStructure';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  profileData: ProfileStructure;
  _srvURL = 'https://salvation.angorance.tech/api/prof/profiles/';
  token: IToken;

  constructor(private http: HttpClient, public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.getServiceData();
  }

  getServiceData() {

    let sToken = sessionStorage.getItem('token');
    this.token = this.getDecodedAccessToken(sToken);
    const useURL = this._srvURL + this.token.salvationId;

    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': sToken
      })
    };
    this.http.get(useURL, httpOption).subscribe((res: ProfileStructure) => {
      this.profileData = res
      console.log("Profile : ", "Success get profile");
    }, (error) => {
      console.log("Profile : ", "Fail get profile");
      console.log("Profile : ", error);
    });
  }

  dateFormat(profileStruct: ProfileStructure) {
    if (profileStruct.birth_date != undefined) {
      let str = profileStruct.birth_date.split('-');
      let day = Number(str[2].substr(0, 2));
      let month = Number(str[1]) - 1;
      let year = Number(str[0]);
      let date = new Date(year, month, day);
      profileStruct.birth_date = date.toLocaleDateString();
    }
  }

  ngOnInit() {
    this.getServiceData();
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(PasswordDialog, {
      width: '350px'
    });

    dialogRef.afterClosed().subscribe((result: String) => {
      if (result && result !== '') {
        let url = 'https://salvation.angorance.tech/api/auth/accounts/' + this.token.salvationId.toString();

        const httpOption = {
          headers: new HttpHeaders({
            'Authorization': sessionStorage.getItem('token')
          })
        };

        this.http.patch(url, { 'password': result }, httpOption).toPromise().then((res) => {

          this.snackBar.open("Password successfully updated", null, {
            duration: 2000,
          });
          console.log("Success update password", "User");
        }, (err) => {
          this.snackBar.open("failed to change your password, try again later or contact an administrator", null, {
            duration: 5000,
          });
          console.log("Fail update password", "User");
          console.log(err, "User");
        });
      }
    });
  }

}

@Component({
  selector: 'password-dialog',
  templateUrl: 'password-dialog.html',
})
export class PasswordDialog {

  password = new FormControl('', [Validators.required]);
  passwordConfirmation = new FormControl('', [Validators.required]);

  constructor(
    public dialogRef: MatDialogRef<PasswordDialog>) { }

  confirmPassword() {

    if (this.password.value === this.passwordConfirmation.value && this.password.value !== '') {
      this.dialogRef.close(this.password.value);
    }
  }
}