import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCreationButtonComponent } from './user-creation-button.component';

describe('UserCreationButtonComponent', () => {
  let component: UserCreationButtonComponent;
  let fixture: ComponentFixture<UserCreationButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCreationButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreationButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
