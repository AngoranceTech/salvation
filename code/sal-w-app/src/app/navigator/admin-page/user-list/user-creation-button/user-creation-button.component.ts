import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { UserCreationFormComponent } from '../user-creation-form/user-creation-form.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-creation-button',
  templateUrl: './user-creation-button.component.html',
  styleUrls: ['./user-creation-button.component.css']
})
export class UserCreationButtonComponent implements OnInit {

  creationFormDialog: MatDialogRef<UserCreationFormComponent>;

  constructor(
    public dialog: MatDialog, ) { }

  ngOnInit() {
  }

  openDialog() {

    console.log('opening user creation dialog');

    this.creationFormDialog = this.dialog.open(UserCreationFormComponent, {
      width: '40%',
      minWidth: '250px',
      autoFocus: false,
    });

    this.creationFormDialog.afterClosed().subscribe(() => {
      console.log('user creation dialog closed')
    });

  }

}
