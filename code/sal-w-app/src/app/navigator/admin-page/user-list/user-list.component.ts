import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { ProfileStructure } from 'src/app/interfaces/IProfileStructure';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';


export interface DialogData {
  _id: string;
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  PROFILE_DATA: ProfileStructure[];
  displayedColumns: string[] = ['last_name', 'mail', 'phone_number', 'action'];
  dataSource;
  _srvURLProfile = 'https://salvation.angorance.tech/api/prof/profiles/';
  _srvURLUser = 'https://salvation.angorance.tech/api/auth/accounts/';


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /**
   * Constructor
   */
  constructor(
    private router: Router,
    private http: HttpClient,
    public dialog: MatDialog,
  ) { }


  ngOnInit() {
    this.getProfiles();
  }

  openConfirmation(id: number): void {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '500px',
      data: { _id: id }
    });

    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getProfiles() {
    const useURL = this._srvURLProfile;
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.get(useURL, httpOption).subscribe((res: ProfileStructure[]) => {
      this.PROFILE_DATA = res;
      this.dataSource = new MatTableDataSource(this.PROFILE_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      console.log("Profiles : ", "Sucess get all profiles")
      
    }, (error) => {
      console.log("Profiles : ", "Fail get all profiles")
      console.log("Profiles : ", error)
    });
  }


  detailUser(id: number) {
    this.router.navigateByUrl('/Nav/Admin/Users/' + id);
  }
  /*deleteFromTable(id : number){
    let index;
    for(let profile of this.PROFILE_DATA){
      if(profile.salvationId == id){
        index = this.PROFILE_DATA.indexOf(profile, 0);
        break;
      }
    }

    if(index > -1){
      this.PROFILE_DATA.splice(index,1);
    }

    this.dataSource = new MatTableDataSource(this.PROFILE_DATA);
    this.dataSource.paginator = this.paginator;
    console.log(this.PROFILE_DATA);
  }*/
}


