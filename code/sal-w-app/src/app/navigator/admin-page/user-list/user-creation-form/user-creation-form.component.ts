import { Component, OnInit, ViewContainerRef } from '@angular/core';

import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { UserListComponent } from '../user-list.component';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { resolve } from 'q';
import { Router } from '@angular/router';
import { WalletStructure } from 'src/app/interfaces/IWalletStructure';
import { ProfileStructure } from 'src/app/interfaces/IProfileStructure';

@Component({
  selector: 'app-user-creation-form',
  templateUrl: './user-creation-form.component.html',
  styleUrls: ['./user-creation-form.component.css']
})
export class UserCreationFormComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  firstname = new FormControl('', [Validators.required]);
  birthdate = new FormControl('', [Validators.required]);
  lastname = new FormControl('', [Validators.required]);
  phone = new FormControl('', [Validators.required]);
  address = new FormControl('', [Validators.required]);
  npa = new FormControl('', [Validators.required]);
  city = new FormControl('', [Validators.required]);
  role = new FormControl('', [Validators.required]);
  plate_number = new FormControl('');
  errorMessage: string;

  srvUrl = "https://salvation.angorance.tech/api";
  id: number;


  constructor(
    private http: HttpClient,
    public dialogRef: MatDialogRef<UserListComponent>,
    private router: Router,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  openNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  dateFormat(date: Date): string {
    return date.toLocaleDateString();
  }

  /*
  function toLowerCase() { [native code] }.function toLowerCase() { [native code] }@heig-vd.ch*/
  createUser() {
    if (this.validateform()) {
      let sToken = sessionStorage.getItem('token');

      const httpOption = {
        headers: new HttpHeaders({
          'Authorization': sToken
        })
      };

      let registerUrl = this.srvUrl + '/auth/registration';
      let regsitrationBody = {
        'email': this.email.value,
        'password': 'password123',
        'role': this.role.value
      };

      this.http.post(registerUrl, regsitrationBody, httpOption).toPromise()
        .then((res: { id: number, email: String, role: String }) => {
          this.id = res.id;
          let profileUrl = this.srvUrl + '/prof/profiles/' + res.id;

          this.birthdate.setValue(this.dateFormat(this.birthdate.value));
          console.log("Birth date : ", this.birthdate);

          let profileBody = {
            'first_name': this.firstname.value,
            'last_name': this.lastname.value,
            'birth_date': this.birthdate.value,
            'address': this.address.value,
            'npa': Number(this.npa.value),
            'city': this.city.value,
            'phone_number': this.phone.value,
            'email': this.email.value,
            'plate_number': this.plate_number.value
          }

          console.log("User : ", "Success creation");
          return this.http.post(profileUrl, profileBody, httpOption).toPromise();
        }, (error) => {
          this.openNotification('Fail creation', null);
          console.log("User : ", "Fail creation");
          console.log("User : ", error);
        }).then(
          (res) => {
            let walletUrl = this.srvUrl + '/wallet/wallets/' + this.id;
            let walletBody = [
              { 'amount': 50.0, 'type': 'main', },
              { 'amount': 20.0, 'type': 'print', },
            ];
            console.log("Profile : ", "Success creation");
            return this.http.post(walletUrl, walletBody, httpOption).toPromise();
          }, (error) => {
            console.log("Profile : ", "Fail creation");
            console.log("Profile : ", error);
            this.deleteUser(this.id);
          }
        ).then((res: WalletStructure[]) => {
          this.router.navigateByUrl('/Nav/Admin', { skipLocationChange: false }).then(() => {
            this.router.navigate(['/Nav/Admin/Users'])
          })
          console.log("Wallet : ", "Success creation");
          this.openNotification('Success creation user', null);
        }, (error) => {
          this.openNotification('Fail creation wallet. Edit the new user to create wallet', null);
          console.log("Wallet : ", "Fail creation")
          console.log("Wallet : ", error);
        })
      this.dialogRef.close();
    } else {
      this.errorMessage = "Veuillez remplir tout les champs et mettre un NPA numérique"
    }

  }
  generateMail() {
    if (this.firstname.value && this.lastname.value) {
      let f: String = this.firstname.value;
      let l: String = this.lastname.value;
      let mail: String = f.toLowerCase() + '.' + l.toLowerCase() + '@heig-vd.ch';

      this.email.setValue(mail.replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
    }
  }

  validateform(): boolean {
    return this.email.valid &&
      this.firstname.valid &&
      this.lastname.valid &&
      this.address.valid &&
      this.npa.valid &&
      !isNaN(Number(this.npa.value)) &&
      this.city.valid &&
      this.role.valid &&
      this.phone.valid;
  }

  deleteUser(id: number) {
    console.log("Delete user " + id);
    const useURL = this.srvUrl + "/auth/accounts/" + id;
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.delete(useURL, httpOption).subscribe(() => {
      console.log("User : ", "Success delete")
    }, (error) => {
      console.log("User : ", "Fail delete")
      console.log("User : ", error)
    });
  }
}
