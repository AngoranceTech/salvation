import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { DialogData } from '../user-list.component';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})
export class DialogConfirmComponent implements OnInit {

  _srvURLProfile = 'https://salvation.angorance.tech/api/prof/profiles/';
  _srvURLUser = 'https://salvation.angorance.tech/api/auth/accounts/';
  _srvURLWallet = 'https://salvation.angorance.tech/api/wallet/wallets/';

  constructor(
    private router: Router,
    private http: HttpClient,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteUser(id: number) {
    console.log("Delete user " + id);
    const useURL = this._srvURLUser + id;
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.delete(useURL, httpOption).subscribe(() => {
      this.openNotification("Sucess delete", "User");
      console.log("User : ", "Success delete")
    }, (error) => {
      this.openNotification("Fail delete", "User");
      console.log("User : ", "Fail delete")
      console.log("User : ", error)
    });
  }

  deleteProfile(id: number) {
    const useURL = this._srvURLProfile + id;
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.delete(useURL, httpOption).subscribe(() => {
      this.openNotification("Success Delete", "Profile");
      this.router.navigateByUrl('/Nav/Admin', { skipLocationChange: false }).then(() =>
        this.router.navigate(["/Nav/Admin/Users"]));
        console.log("Profile : ", "Success delete")
    }, (error) => {
      this.openNotification("Fail Delete", "Profile");
      console.log("Profile : ", "Fail delete")
    });
  }

  deleteWallets(id:number){
    const useURL = this._srvURLWallet + id;

    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    this.http.delete(useURL, httpOption).subscribe(() => {
      this.openNotification("Success Delete", "Wallet");
      this.router.navigateByUrl('/Nav/Admin', { skipLocationChange: false }).then(() =>
        this.router.navigate(["/Nav/Admin/Users"]));
        console.log("Wallets : ", "Success delete");
    }, (error) => {
      this.openNotification("Fail Delete", "Wallet");
      console.log("Wallets : ", "Fail delete")
    });

  }

  delete(id: number) {
    this.deleteUser(id);
    this.deleteProfile(id);
    this.deleteWallets(id);
  }

  openNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  ngOnInit() {
  }

}
