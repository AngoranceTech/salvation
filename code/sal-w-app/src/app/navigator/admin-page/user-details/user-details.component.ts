import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';
import { ProfileStructure } from 'src/app/interfaces/IProfileStructure';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { UserStructure } from 'src/app/interfaces/IUserStructure';
import { WalletStructure } from 'src/app/interfaces/IWalletStructure';
import { formatDate } from '@angular/common';
import { isUndefined } from 'util';


export interface Role {
  value: string;
  valueView: string;
}

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  profile: ProfileStructure;
  _srvURLProfile = 'https://salvation.angorance.tech/api/prof/profiles/';
  _srvURLUser = 'https://salvation.angorance.tech/api/auth/accounts/';
  _srvURLWallet = 'https://salvation.angorance.tech/api/wallet';
  profileData: ProfileStructure;
  userData: UserStructure;
  id: string;
  panelOpenState = false;
  selected;
  wallets: WalletStructure[] = [];

  email: FormControl;
  role: FormControl;
  first_name: FormControl;
  last_name: FormControl;
  address: FormControl;
  npa: FormControl;
  city: FormControl;
  phone_number: FormControl;
  birth_date: FormControl;
  plate_number: FormControl;

  amount = new FormControl('', [Validators.required]);
  type = new FormControl('main', [Validators.required]);
  errorMessage: String;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    public snackBar: MatSnackBar,
  ) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id')
    });
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' : '';
  }

  ngOnInit() {
    this.getProfileByID();
    this.getUserByID();
    this.getWalletsByID();
  }



  getProfileByID() {
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };
    const useURL = this._srvURLProfile + this.id;

    this.http.get(useURL, httpOption).subscribe((res: ProfileStructure) => {
      this.profileData = res;
      console.log("Profile :", this.profileData);
      this.setProfileDataForm();
      console.log("Profile : ", "Success get profile by id");
    }, (error) => {
      console.log("Profile : ", "Fail get profile by id");
      console.log("Profile : ", error);
    });
  }

  getUserByID() {
    let token = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };
    const useURL = this._srvURLUser + this.id;

    this.http.get(useURL, httpOption).subscribe((res: UserStructure) => {
      this.userData = res;
      this.setUserDataForm();
      this.selected = res.role;
      console.log("User : ", "Success get profile by id");
    }, (error) => {
      console.log("User : ", "Fail get profile by id");
      console.log("User : ", error);
    });
  }

  update() {
    if (this.hasChangeFieldUser()) {
      this.updateUser();
    }

    if (this.hasChangeFieldProfile()) {
      this.updateProfile();
    }
  }

  updateProfile() {
    let token = sessionStorage.getItem('token');

    if (!isNaN(Number(this.npa.value))) {
      let body = {
        'salvationId': this.profileData.salvationId,
        'first_name': this.first_name.value,
        'last_name': this.last_name.value,
        'email': this.email.value,
        'address': this.address.value,
        'npa': Number(this.npa.value),
        'city': this.city.value,
        'phone_number': this.phone_number.value,
        'birth_date': this.birth_date.value,
        'plate_number': this.plate_number.value
      }

      const httpOption = {
        headers: new HttpHeaders({
          'Authorization': token
        })
      };
      const useURL = this._srvURLProfile + this.id;

      this.http.patch(useURL, body, httpOption).subscribe(() => {
        this.openNotification("Success update", null);
        this.profileData = body;
        console.log("Profile : ", "Success update");
      }, (error) => {
        this.openNotification("Profile : Fail update", null);
        console.log("Profile : ", "Fail update");
        console.log("Profile : ", error);
      });
    } else {
      this.openNotification("Fail : Veuillez entrez un NPA numérique !", null);
    }

  }

  updateUser() {
    let token = sessionStorage.getItem('token');

    let body = {
      'salvationId': this.userData.salvationId,
      'email': this.email.value,
      'role': this.role.value,
    }

    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };
    const useURL = this._srvURLUser + this.id;

    this.http.patch(useURL, body, httpOption).subscribe(() => {
      this.userData = body;
      this.openNotification("Success update", null);
      console.log("User : ", "Success update");
    }, (error) => {
      this.openNotification("User : Fail update", null);
      console.log("User : ", "Fail update");
      console.log("User : ", error);
    });

  }

  hasChangeFieldUser(): boolean {
    if (this.userData.role !== this.role.value) {
      return true;
    } else if (this.userData.email !== this.email.value) {
      return true;
    }

    return false;
  }

  hasChangeFieldProfile(): boolean {
    if (this.profileData.first_name !== this.first_name.value) {
      return true;
    } else if (this.profileData.last_name !== this.last_name.value) {
      return true;
    } else if (this.profileData.email !== this.email.value) {
      return true;
    } else if (this.profileData.address !== this.address.value) {
      return true;
    } else if (this.profileData.npa !== this.npa.value) {
      return true;
    } else if (this.profileData.city !== this.city.value) {
      return true;
    } else if (this.profileData.phone_number !== this.phone_number.value) {
      return true;
    } else if (this.profileData.birth_date !== this.birth_date.value) {
      return true;
    }

    return false;
  }

  openNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  setProfileDataForm() {
    this.first_name = new FormControl(isUndefined(this.profileData.first_name) ? "" : this.profileData.first_name, [Validators.required]);
    this.last_name = new FormControl(isUndefined(this.profileData.last_name) ? "" : this.profileData.last_name, [Validators.required]);
    this.email = new FormControl(isUndefined(this.profileData.email)? "" : this.profileData.email, [Validators.required, Validators.email]);
    this.address = new FormControl(isUndefined(this.profileData.address) ? "" : this.profileData.address);
    this.npa = new FormControl(isUndefined(this.profileData.npa) ? "" : this.profileData.npa);
    this.city = new FormControl(isUndefined(this.profileData.city) ? "" : this.profileData.city);
    this.phone_number = new FormControl(isUndefined(this.profileData.phone_number) ? "" : this.profileData.phone_number);
    this.birth_date = new FormControl(isUndefined(this.profileData.birth_date) ? "" : this.profileData.birth_date);
    this.plate_number = new FormControl(isUndefined(this.profileData.plate_number) ? "" : this.profileData.plate_number);
  }

  setUserDataForm() {
    this.role = new FormControl(this.userData.role, [Validators.required]);
  }

  getWalletsByID() {
    let sToken = sessionStorage.getItem('token');
    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': sToken
      })
    };
    let walletUrl = this._srvURLWallet + "/wallets/" + this.id;
    this.http.get(walletUrl, httpOption).subscribe((res: WalletStructure[]) => {
      this.wallets = res;
      console.log("Wallet : ", "Success get wallets");
    }, (error) => {
      console.log("Wallet : ", "Fail get wallets");
      console.log("Wallet : ", error);
    });
  }

  createWallets() {
    let sToken = sessionStorage.getItem('token');
    let walletUrl = this._srvURLWallet + "/wallets/" + this.id;

    let walletBody = [
      { 'amount': 50.0, 'type': 'main', },
      { 'amount': 20.0, 'type': 'print', },
    ];

    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': sToken
      })
    };
    this.http.post(walletUrl, walletBody, httpOption).subscribe((res: WalletStructure[]) => {
      this.wallets = res;
      console.log("Wallet : ", "Success get wallets");
    }, (error) => {
      console.log("Wallet : ", "Fail get wallets");
      console.log("Wallet : ", error);
    })
  }

  topUp() {
    const useURL = this._srvURLWallet + "/topup/" + this.id;
    let token = sessionStorage.getItem('token');

    const httpOption = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };


    let body = {
      "amount": this.amount.value,
      "type": this.type.value,
    }
    if (this.validateform()) {
      this.http.post(useURL, body, httpOption).subscribe((res) => {
        console.log("Top up : ", "Success");
        this.openNotification("Top up : Success", null);
      }, (error) => {
        console.log("Top up : ", "Fail");
        this.openNotification("Top up : Fail", null);
        console.log("Top up : ", error);
        this.errorMessage = error.error;
      });
    } else {
      this.errorMessage = "Veuillez remplir tout les champs ou veuillez mettre un montant plus grand que 0"
    }
  }

  validateform(): boolean {

    return this.amount.value > 0 &&
      this.amount.valid;
  }
}
