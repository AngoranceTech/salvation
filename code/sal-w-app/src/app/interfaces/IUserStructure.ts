export interface UserStructure {
    salvationId: number,
    email: string,
    role : string
};