export interface WalletStructure {
    _id: string,
    amount: number,
    type : string
};