export interface TransactionStructure {
    amount: number,
    from: {
        salvationId:number,
        name:string,
    },
    to:{
        salvationId:number,
        name:string,
    },
    description:string,
    date:Date;
};