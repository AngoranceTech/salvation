export interface ProfileStructure {
    salvationId: number,
    first_name: string,
    last_name: string,
    birth_date: string,
    address: string,
    npa: number,
    city: string,
    phone_number: string,
    email: string,
    plate_number:string
};