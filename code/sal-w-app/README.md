# Sal-w-app

## Introduction
The web application is the main portal of the project. It allows the users to use our services, and the administrator to manage the users and data. The different services proposed are the following : 

 - Authentication
 - Profile
 - Wallet
 - Admin
 ## Technology used

We made this web application with angular 6. It uses as programming language typescript. This technology allows us to create dynamic and immersive web applications. It allows us to simply create pages with automatic module / component generation.

 ## Structure of the web application
Our application is broken down into module and component. The modules allow us to manage the routes to the different modules and component that compose it. Our structure looks like this:

 - app (module)
   - login-form (composant)
   - navigator (module)
     - dashboard-page (composant)
     - profile-page (composant)
     - wallet-page (composant)
     - access-page (composant)
     - admin-page (module)
       - user-list(composant)
       - user-detail (composant)
       - doors (composant)
       - groups (composant)

Each of its modules and component represents an element of our web application.

## Presentation of our application

![login](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal-w-app/image/login-page.PNG)

![profile](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal-w-app/image/profile-page.PNG)

![wallet](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal-w-app/image/wallet-page.PNG)

![admin](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal-w-app/image/admin-page.PNG)

![edit](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal-w-app/image/edit-page.PNG)

![edit](https://gitlab.com/AngoranceTech/salvation/raw/master/code/sal-w-app/image/edit-user-page-wallet.PNG)

## What can we do with our application?
In our application we have two modes of operation. A mode for the standard users and a mode for an administrator. An administrator is an upgraded user, it can access to the same features as a regular user but has also access to additional features.

### Standard user

#### Profile page

A typical user can authenticate to our application in order to see the information of his account. He can also go on his profile page to see information about him:
- Lastname / Fristname
- Address
- Phone number
- Birth date
- Plate number (car)

It also allows us to change his password. The user does not have the possibility to change the other information of his account. Only the administrator can do it.

#### Wallet page

In the wallet page we can find the information of the two wallets (main and print), the amount of the wallet and a transaction table of the wallet. In this page we also have the possibility to give money to another user via their email address.

### Administrator

#### Admin page
The adminstrator, as said above, has the same features as the classic user but with additional features. These features are present in the admin page which is accessible only by an administrator. In this page we have the list of all users. There is a search box to look for a user more easily. Via this list we have the possibility to delete or edit a user.

We can also create a user via this page. The creation button will display a form allowing the creation of the user. During the creation we create the user account, its profile as well as its two wallets.

#### Edit user page
The Administrator will be able via this page, to edit all the information of the account of the user to select. In addition they will be able to top up the wallet of this user. If the wallets hasn't been created, the administrator can create them via this pages.

## Application testing
To test our web application, we use codecepts. This application uses the javascript programming language. It allows us to automate tests according to a given senario. We decide what to do the application such as fill fields, click a button, check on which url we are in order to say whether the actions are fair or not.
