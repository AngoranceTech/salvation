# Mobile Development Analysis

Author: Daniel Gonzalez Lopez

## What is the problematic?

In the mobile phones world, there are many different ways of developping an app. The first one is to develop native applications, for example using Android Studio and Java for Android or Xcode and Swift for iOS. The problem is that it takes time and ressources because you need Android and iOS devs. Not everyone can have both, the apps between both OS are not going to be similar. Start-ups needs to reach a maximum of people and it can be difficult because of that.  

These last years, lots of Frameworks came and took place that solved this problem. React Native, brought by *Facebook*, is one of them (to speak of the most used) and allows developpers to build apps for Android and iOS using... JavaScript and React! So one programming language, both Android and iOS final applications.  

## What are we looking for?

Here, we are going to compare two interesting Frameworks for building apps on both Android and iOS so that we can decide if it is interesting for our student project to use one of them or to simply develop native Android apps.  

The first Framework is **React Native**, the most used and known over the last years. The second one is **Flutter**, brought by *Google* as a competitive concurrent of React Native that is supposed to make more "native orriented" apps for both systems and thus be more performing.  

My goal here is to present to my team (and you all) both Frameworks and give my opinion on which solution take for our project (Native Android, Flutter or React Native?).  

## Description

### React Native

**React Native** is a Framework to build native apps on both Android and iOS using mainly **JavaScript**. It does not create "web apps", "HTML5 apps" or else but native apps. It has a **hot reloading** features that allows a quicker development as you do not need to recompile your code when you want to apply changes. And if needed, you can add native code to optimise some parts of the app.  

From what I have seen, it is quite easy to get started with, the guides are clear, comprehensive and easy to follow. Another advantage of it is its community. React Native has been used from 2015 and has a big community that can help to resolve issues, there are already tons of forums and articles about it, as well as tutorials.

### Flutter

**Flutter** is a Framework to build native apps on both Android and iOS using Google's programming language **Dart**. As React Native, Flutter create native apps and not "web apps" or else. It has a **hot reloading** feature too and set the animation standard at 60fps. You can add native code if needed.  

It is easy to start using Flutter too, the guides and tutorials are well written and clear, there are examples and lots of documentation. The community is smaller but I have heard the Flutter team at Google is quite active to help devs when they have question and problems.

### React Native vs Flutter

If I have to compare now Flutter and React Native, I would focus on some little differencies. Globally, they are both very capable and interesting. But there are three big differencies:

**Community**  
    React Native has clearly a better community as it has been here for longer time. So when it comes to search docs and info about issues, it will be easier if working on React Native.

**Performance**  
    Flutter has clearly better performances. First of all, they set the animation standard to 60fps and it is amazingly high (for app development). Even when programming on native Android, some apps do not get that animation speed. Then, it has a better structure (mostly because of Dart and its web / mobile app orientation) that makes Flutter better for performance.

**UI**  
    Speaking of UI, React Native has to use external libraries for UI components when Flutter has already his components packed with everything else. It is details, but when you never used both of them, the fact that one of them do not need external libraries is quite a good point.

Now, there are other differencies as it goes, but other articles will explain them better.

### What suits better for our project?

Technically speaking, both of them would suit for our project. The app we want to develop is more of a "view" app than anything else and do not require lots of performance or else. React Nativ would be easier to start with considering we never learnt **Dart** programming language, but then even if we know some **JavaScript** basis, we never used it a lot nor used libraries with it, therefore both could be hell or heaven.

### Which is more interesting educationally speaking?

Both would be very interesting to learn and use. As we do not know much about it, we still have everything to learn from. But I personally am more attracted by Flutter than React Nativ, the main reason is its programming language **Dart**. After reading lots of articles about Flutter and its use of Dart, I am clearly intrigued by it as it was developped and designed to perform really well for web and mobile apps.